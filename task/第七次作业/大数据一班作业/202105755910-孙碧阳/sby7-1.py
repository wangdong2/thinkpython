import math
def mysqrt(a, x):
    while True:
        y=(x+a/x)/2
        if y==x:
            break
        x=y
    return y

def test_square_root():
    x=2
    for a in range(1,10):
        s = mysqrt(a, x)
        b = math.sqrt(a)
        y = abs(s-b)
        print(a,s,b,y)
  
print("a     mysqrt(a)     math.sqrt(a)      diff")
print("-     --------      -----------       ----")
test_square_root()