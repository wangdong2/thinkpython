import math
 
def estimate_pi():
    cnt = 2*math.sqrt(2) / 9801
    k = 0
    tie = 0
    while True:
        item = math.factorial(4*k) * (1103+26390*k) / (math.factorial(k) ** 4) / (396 ** (4*k))
        tie = tie + item
        k = k + 1
        if item < 1e-15:
            break
    pi = 1 / (tie*cnt)
    print('my_ans ==',pi)
    print('math.pi_ans ==',math.pi)
 
estimate_pi() 