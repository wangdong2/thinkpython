def square_root(x,a):
    while True:
        print(x)
        y=(x+a/x)/2
        if y==x:
            break
        x=y
    return x
square_root(2,4)    



import math
def estimate_pi():
    total = 0
    k = 0
    factor = 2 * math.sqrt(2) / 9801
    while True:
        num = factorial(4*k) * (1103 + 26390*k)
        den = factorial(k)**4 * 396**(4*k)
        term = factor * num / den
        total += term
        
        if abs(term) < 1e-15:
            break
        k += 1

    return 1 / total

print(estimate_pi())



第一个对  表示如果字符串中包含至少一个区分大小写的字符，并且所有这些(区分大小写的)字符都是小写，则返回 True，否则返回 False
          当有一个是小写的时候，由于return的存在，程序被退出
    
第二个对  表示‘c’肯定是小写形式，该结果永恒正确,即使形参都是大写
第三个错  表示 对于每一个字母都可以得到正确判断，但是每一次的结果会被刷新，只会保存最后一次的结果,打印所有结果即可观察
第四个对  表示 ，通过or操作，会将结果为真的保留下来
第五个错  表示遇到结果为大写的时候，函数被停止，返回错误结果


def rotate_letter(letter, n):
 
    if letter.isupper():
        start = ord('A')
    elif letter.islower():
        start = ord('a')
    else:
        return letter

    c = ord(letter) - start
    i = (c + n) % 26 + start
    return chr(i)


def rotate_word(word, n):
    res = ''
    for letter in word:
        res += rotate_letter(letter, n)
    return res


if __name__ == '__main__':
    print(rotate_word('cheer', 7))
    print(rotate_word('melon', -10))
    print(rotate_word('sleep', 9))