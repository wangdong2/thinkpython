import math

def mysqrt(a):
    x = a/2
    while True:
        y = (x + a/x) / 2
        if abs(y-x) < 0.0000000000000000000000001:
            break
        x = y
    return x

def test_squre_root(a):
    nat = "{:<4}\t{:<16}\t{:<16}\t{:<16}"
    mat = "{:.1f}\t{:<16}\t{:<16}\t{:<16}"
    print(nat.format('a', 'mysqrt(a)', 'math.sqrt(a)', 'diff'))
    print(nat.format('-', '---------', '------------', '----'))
    for i in range(a):
        my_sqrt = mysqrt(i+1)
        math_sqrt = math.sqrt(i+1)
        diff = abs(my_sqrt - math_sqrt)
        print(mat.format(i+1, my_sqrt, math_sqrt, diff))
 
test_squre_root(9)