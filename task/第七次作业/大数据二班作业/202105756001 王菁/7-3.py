import math

def factorial(k):
    f = 1
    i = 1
    while True: 
        f = f*i
        i = i + 1
        if i > k:
            break
    return f

def delta(k):
    d = (factorial(4*k)*(1103 + 26390*k)) / ((factorial(k))**4*396**(4*k))
    return d

def xigama():
    i = 0
    x = 0
    while True:
        if delta(i) < 1e-15:
            break
        x = x + delta(i)
        i = i + 1	
    return x

def estimate_pi():
    estimate_pi = 9801/(2*math.sqrt(2)*xigama())
    return estimate_pi

print('estimate_pi =',estimate_pi())
print('epsilon =', (math.pi-estimate_pi()))