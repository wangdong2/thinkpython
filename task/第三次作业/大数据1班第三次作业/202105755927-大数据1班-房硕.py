#!/usr/bin/env python
# coding: utf-8

# # 练习3-3

# Q1

# In[51]:


def print_row():
    return print("+ - - - -" ,end=' ')


# In[52]:


def print_line():
    return print("|         " ,end='')


# In[53]:


def print_line2():
    return print("|         ")


# In[54]:


print_row()
print_row()
print("+")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_row()
print_row()
print("+")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_line()
print_line()
print("|")
print_row()
print_row()
print("+")


# Q2

# In[49]:


print_row()
print_row()
print_row()
print_row()
print("+")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_row()
print_row()
print_row()
print_row()
print("+")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_row()
print_row()
print_row()
print_row()
print("+")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_row()
print_row()
print_row()
print_row()
print("+")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_line()
print_line()
print_line()
print_line()
print("|")
print_row()
print_row()
print_row()
print_row()
print("+")


# In[ ]:




