import random
def nested_sum(t):
 total = 0
    for nested in t:
        total += sum(nested)
    return total