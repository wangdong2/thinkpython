def cumsum(t):
    res = []
    total = 0
    for s in t:
        total += s
        res = res + [total]
    return res


t = [1, 2, 3]
print(cumsum(t))