import turtle
def koch(t,x):
    '''
    t: Turtle类型变量
    x: 科赫曲线的参考长度
    -----
    该函数画出一条科赫曲线
    '''
    if x < 10:
        t.fd(x)
        return
    m = x/3
    koch(t, m)
    t.lt(60)
    koch(t, m)
    t.rt(120)
    koch(t, m)
    t.lt(60)
    koch(t, m)
    
def snowflake(t,x):
    '''
    t: Turtle类型变量
    x: 科赫曲线的参考长度
    -----
    该函数画出三条长度为x的科赫曲线组成一个雪花形状
    '''
    for i in range(3):
        koch(t,x)
        t.rt(120)

bob = turtle.Turtle()
bob.speed(0)
bob.hideturtle()
x = 360
snowflake(bob,x)
turtle.mainloop()
