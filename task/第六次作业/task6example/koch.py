import turtle

def koch(t,x):
    '''
    t: Turtle类型变量
    x: 科赫曲线的参考长度
    -----
    该函数画出一条科赫曲线
    '''
    if x < 10:
        t.fd(x)
        return
    m = x/3
    koch(t, m)
    t.lt(60)
    koch(t, m)
    t.rt(120)
    koch(t, m)
    t.lt(60)
    koch(t, m)

bob = turtle.Turtle()
bob.speed(0)
bob.hideturtle()
x = 360
koch(bob,x)
turtle.mainloop()
