def ackermann(m,n):
    '''
    m:一个大于等于0的整数
    n:一个大于等于0的整数
    ------
    该函数实现Ackermann函数的功能
    '''
    if m<0 or n<0:
        print('输入的数字小于0')
        return
    
    m = int(m)
    n = int(n)
    while True:
        if m==0:
            return n+1
        elif n==0:
            return ackermann(m-1,1)
        else:
             return ackermann(m-1,ackermann(m,n-1))

m = 3
n = 4
print(ackermann(m,n))
print('当m和n的值较大时(实际上3,4应该是能取到的最大的值了，都取4都运行不出来)')
print('会报错:RecursionError: maximum recursion depth exceeded in comparison')
print('即递归次数大于最大递归深数,实际上python的递归深度是有限制的，默认为1000，不过可以手动调整递归深度')
# 当m和n的值
