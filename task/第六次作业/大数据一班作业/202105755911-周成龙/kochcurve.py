import turtle#导入乌龟模块#
def koch(bob, n):#定义koch函数#
    if n<3:#小于3的情况#
        bob.fd(n)
        return
    koch(bob,n/3.0)#照书上的递归#
    bob.lt(60)
    koch(bob, n/3.0)
    bob.rt(120)
    koch(bob, n/3.0)
    bob.lt(60)
    koch(bob, n/3.0)
def snowflake(bob, n):#定义snowflake函数#
    for i in range(3):#循环3次#
        koch(bob, n)
        bob.rt(120)
snowflake(turtle.Turtle(), 300)
