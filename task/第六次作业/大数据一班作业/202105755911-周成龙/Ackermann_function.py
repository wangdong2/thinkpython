def ack(m,n):#定义函数#
    if (m==0):#循环循环循环#
        return (n+1)
    if (m>0 and n==0):
        return ack(m-1,1)
    if (m>0 and n>0):
        return ack(m-1,ack(m,n-1))
a = ack(3,4)
print(a)
#如果m,n很大，则会出现“RecursionError: maximum recursion depth exceeded in comparison”#
