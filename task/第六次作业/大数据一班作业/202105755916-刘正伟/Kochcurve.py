import turtle

bob = turtle.Turtle()
bob.delay=0.001

def koch(t,n):
    if n <11:
        t.fd(n)
        return
    length = n/3
    koch(t,length)
    t.lt(60)
    koch(t,length)
    t.rt(120)
    koch(t,length)
    t.lt(60)
    koch(t,length)

def snowflake(t,n):
    for i in range(3):
        koch(t,n)
        t.rt(120)
snowflake(bob,333)
turtle.mainloop()
