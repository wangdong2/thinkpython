import math#引入math
def ack(m,n):#定义ack
    if m==0:#判断m是否等于0
        return(n+1)#如果等于就返回，让n+1代替原来的n
    elif (m>0 and n>0):#判断m是否大于0并且n大于0，下同
        return ack(m-1,ack(m,n-1))#如果符合，就返回，下同
    else:
        if(m>0 and n==0):
            return ack(m-1,1)

ack(3,4)#调用函数

#答：若m，n很大，则因递归深数有限，超出限度无法输出¶