import math#引入math
import turtle#引入turtle
t=turtle.Turtle()#将turtle用t代替
turtle.delay=1#加快速度

def koch(t,x):#定义koch使之能画出一段科赫曲线
    if x<3:#当x小于三画直线
        t.fd(x)
    if x>=3:#当x大于三
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)
        t.rt(120)
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)

t.penup()
t.goto(-250,150)
t.pendown()
koch(t,100)#画一个指定长度的koch

t.penup()
t.goto(-250,0)
t.pendown()

def koch_pro(t,n,x):#泛化koch
    if x<3:#这条曲线每条小线段最多长1，一小段科赫曲线最小为3
        t.fd(x)
    if x>=3:
        if n==1:#判断n与1大小
            t.fd(x/3)
            t.lt(60)
            t.fd(x/3)
            t.rt(120)
            t.fd(x/3)
            t.lt(60)
            t.fd(x/3)
        else:#递归，能根据不同的n将koch做的更细致
            koch_pro(t,n-1,x/3)#每一次的长度都是上一次的三分之一，最小到1，下同
            t.lt(60)#左转60，下类似
            koch_pro(t,n-1,x/3)
            t.rt(120)
            koch_pro(t,n-1,x/3)
            t.lt(60)
            koch_pro(t,n-1,x/3)

koch_pro(t,10000,10000)#画koch曲线

t.penup()
t.goto(-250,-150)
t.pendown()
def snowflake(t,n,x): #定义雪花
    for i in range (3):#绘制三条科赫曲线，组成雪花
        koch_pro(t,n,x)
        t.rt(120)
snowflake(t,4,81)

turtle.mainloop()
turtle,exitonclick()
            