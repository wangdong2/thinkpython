## 大数据一班 刘子昂 202105755015
import turtle
t=turtle.Turtle()
def Koch(t,x):
    if x<3:
        t.fd(x)
    else:
        Koch(t,x/3)
        t.lt(60)
        Koch(t,x/3)
        t.rt(120)
        Koch(t,x/3)
        t.lt(60)
        Koch(t,x/3)

def snowflake(t,x):
    Koch(t,x)
    t.rt(120)
    Koch(t,x)
    t.rt(120)
    Koch(t,x)
        
    
turtle.mainloop() 