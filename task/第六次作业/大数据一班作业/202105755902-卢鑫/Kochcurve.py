import turtle
n = int(input('想要的曲线精度（数字越小精度越高，建议1以上）：'))
def koch(t,x,n):           #绘制kochcurve,n为泛化，可以表示分形曲线精度
    if x < 3*n:
        t.fd(x)
    else:                #递归制造分形
        x = x/3
        koch(t, x, n)
        t.lt(60)
        koch(t, x, n)
        t.rt(120)
        koch(t, x, n)
        t.lt(60)
        koch(t, x, n)
def snowflake(t,x,n):      #画雪花
    #调整位置，使雪花位于中间
    t.pu()
    t.lt(90)
    t.fd(104)
    t.lt(90)
    t.fd(125)
    t.rt(180)
    t.pd()
    #根据几何关系寻找每条Kochcurve位置
    for i in range(3):
        koch(t, x, n)
        t.rt(120)
turtle.speed(0)
turtle.delay(0)
t = turtle.Turtle()
turtle.hideturtle()
snowflake(t,250,n)
turtle.mainloop()
