import turtle

bob=turtle.Turtle()

def Koch(t,m): #绘制科赫曲线
    if m<3:  #起限制循环次数作用
        t.forward(m)
    else:
        for i in [0,60,-120,60]: #形成连续的小三角形
            t.lt(i)
            Koch(t,m/3)
            
def Snowflack(t,m): #绘制雪花
    for p in range(3):
        Koch(t,m)
        t.rt(120)
        
        
bob.screen.delay(0)

bob.hideturtle()

Snowflack(bob,300)
  
turtle.mainloop()    