##练习5—6
import turtle
bob=turtle.Turtle()
bob.speed(0)##设置画笔速度
bob.setheading(180)##重置画笔方向，向西
bob.penup()##抬起画笔
bob.fd(300)##向前移动300
bob.setheading(0)##重置画笔方向，向东
bob.pendown()##落下画笔
##练习5—6（1）
def koch(t,x):
    """ t:接收一个turtle
    x:形参，长度
    """
    m=x/3 ##递归，并且每一次长度都是上一次的1/3
    if x<3: ##由题意可得：当x<3时，直接绘制一条长度为x的直线
        t.fd(x)
    else:
        koch(t,m)
        t.lt(60)
        koch(t,m)
        t.rt(120)
        koch(t,m)
        t.lt(60)
        koch(t,m)
koch(bob,200)##绘制一段长度为200的科赫曲线
bob.penup()##抬起画笔
bob.fd(100)##向前移动100
bob.pendown()##画笔落下
##练习5—6（2）
bob.hideturtle()##隐藏画笔箭头
def snowflake(t,x):
    """t:turtle
    x:组成雪花中的每条科赫曲线的长度
    """
    koch(t,x)##画第一条科赫曲线
    t.rt(120)##向右转120°，为画下一条科赫曲线作准备
    koch(t,x)##画第二条科赫曲线
    t.rt(120)##向右转120°，为画下一条科赫曲线作准备
    koch(t,x)##画第三条科赫曲线
snowflake(bob,200)##调用函数snowflake并且输入长度200作为实参
bob.rt(120)##向右转120°
bob.penup()##抬起画笔
bob.fd(300)##向前移动300
bob.pendown()##落下画笔
bob.showturtle()##显示画笔箭头
##练习5—6（3）
def koch(t,x):
    m=x/3
    if x<3:
        t.fd(x)
    else:
        koch(t,m)
        t.lt(60)
        koch(t,m)
        t.rt(120)
        koch(t,m)
        t.lt(60)
        koch(t,m)
koch(bob,200)##以长度x作为形参，并输入200作为实参
turtle.mainloop()