##练习6—2
##(1)计算ack(3,4)
##A(m,n)
def ack(m,n):
    """变量m,n
    """
    if m==0:##如果m=0
        return n+1##则A(m,n)等于n+1
    elif m>0 and n==0:##如果m>0并且n=0
        return ack(m-1,1)##则A(m,n)递归至A(m-1,1)
    elif m>0 and n>0:##如果m>0并且n>0
        return ack(m-1,ack(m,n-1))##则A(m,n)递归至A(m-1,A(m,n-1))
    else:##如果都不符合以上情况
        return None##返回None
ack(3,4)##以书上习题计算ack(3,4)为例，输入3，4作为实参并运行
##(2):对于很大的数字m和n，在运算过程中会占据更多的内存