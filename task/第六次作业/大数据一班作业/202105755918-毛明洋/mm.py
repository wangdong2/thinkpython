#6-2
def ack(m,n):
    if m<0 or n<0:
        print("Ack is not defined for negative numbers.")
        return None
    elif m==0:
        return n+1
    elif m>0 and n==0:
        return ack(m-1,1)
    else:
        return ack(m-1,ack(m,n-1))
ack(3,4)
ack(10,10)#值太大会挂掉