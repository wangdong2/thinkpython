#5.6 毛明洋
import turtle

def koch(t,len,n):
    if n == 0:
        t.fd(len)
    else:
        for i in[0,60,-120,60]:
            t.lt(i)
            koch(t,len/3,n-1)
            
bob=turtle.Turtle()

lenth = 120
level = 3
du = 120
def snowflake():
    koch(bob,lenth,level)
    bob.rt(du)
    koch(bob,lenth,level)
    bob.rt(du)
    koch(bob,lenth,level)
    bob.rt(du)
    turtle.done()
    
snowflake()