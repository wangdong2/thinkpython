#大数据一班
#焦城智
#202105755930
import turtle
bob=turtle.Turtle()
bob.delay=0.00001

def koch_curve(x,bob):#一段科赫曲线
    if x>=3:
        koch_curve(x/3,bob)
        bob.lt(60)
        koch_curve(x/3, bob)
        bob.rt(120)
        koch_curve(x/3, bob)
        bob.lt(60)
        koch_curve(x/3, bob)
    else:
        bob.fd(x)
def sonwflake(m,bob):#三段科赫曲线画一个雪花
    koch_curve(m, bob)
    bob.rt(120)
    koch_curve(m, bob)
    bob.rt(120)
    koch_curve(m, bob)
sonwflake(100, bob)#m可以调节雪花的大小