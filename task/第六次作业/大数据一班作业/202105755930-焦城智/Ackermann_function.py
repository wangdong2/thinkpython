#大数据一班
#焦城智
#202105755930
#如果数字过大将会出现RecursionError: maximum recursion depth exceeded in comparison
def ack(m,n):   
    if m==0:
        return n+1
    elif m>0 and n==0:
        return ack(m-1, 1)
    elif m>0 and n>0:
        return ack(m-1, ack(m,n-1))
    else:
        pass
ack(3,4)