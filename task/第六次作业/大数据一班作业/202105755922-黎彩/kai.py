import turtle
t=turtle.Turtle()
def koch(n,x):
    if n==0:
        t.fd(x)
        return
    elif n==1:
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)
        t.rt(120)
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)
    else:
        koch(n-1,x/3)
        t.lt(60)
        koch(n-1,x/3)
        t.rt(120)
        koch(n-1,x/3)
        t.lt(60)
        koch(n-1,x/3)
t.down()
koch(4,300)
    
    
