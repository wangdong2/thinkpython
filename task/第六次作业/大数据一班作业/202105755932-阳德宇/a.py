#大数据一班阳德宇202105755932
import turtle
def koch(t, n):    
    if n < 10:
        t.fd(n)
        return
    m = n/3
    koch(t, m)
    t.lt(60)
    koch(t, m)
    t.rt(120)
    koch(t, m)
    t.lt(60)
    koch(t, m)

bob = turtle.Turtle()
bob.pu()
bob.goto(-350, 30) 
bob.pd()
koch(bob, 900)
turtle.mainloop()