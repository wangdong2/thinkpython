"""
202105755919-大数据1班-刘一帆-练习六
"""
def ack (m,n):
    if m > 0 and n>= 0:
        if n > 0:
            return ack(m - 1,ack(m,n-1))
        elif n == 0:
            return ack(m - 1,1)
    elif m == 0:
        g = n + 1
        return g
    else:
        print("所给m或n不在函数定义域内")
ack(m = 3,n = 4)
#对于很大的数字，由于重复次数过多，程序会崩溃（报错）。         
