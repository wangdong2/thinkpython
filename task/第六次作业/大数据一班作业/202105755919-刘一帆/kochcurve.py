"""
202105755919-大数据1班-刘一帆-练习五
"""
import turtle
import math
bob= turtle.Turtle()
"""
第一小题
"""
def koch (kehe,x):
    def f1(kehe,x):#先封装一个科赫曲线的代码，以便后面使用
         x = x / 3
         kehe.fd(x)
         kehe.lt(60)
         kehe.fd(x)
         kehe.rt(120)
         kehe.fd(x)
         kehe.lt(60)
         kehe.fd(x)
    if x < 30:#为了更清晰的显示出x < 3 与x >= 3之间的差距，我把题中的3改为了30
        f1(bob,x)
    else:
        f1(bob,x/3)
koch (kehe = bob,x = 90)

bob.pu()
bob.fd(50)
bob.pd()
"""
第二小题
"""
def snowflake (kehe,x):
    def basic1(kehe,x):#利用第1题中的曲线构造更复杂的曲线，并用所得的曲线构造更复杂的曲线
        koch(kehe,x)
        kehe.lt(60)
        koch(kehe,x)
        kehe.rt(120)
        koch(kehe,x)
        kehe.lt(60)
        koch(kehe,x)
    def basic2(kehe,x):
        basic1(kehe,x)
        kehe.lt(60)
        basic1(kehe,x)
        kehe.rt(120)
        basic1(kehe,x)
        kehe.lt(60)
        basic1(kehe,x)
    kehe.rt(120)
    for i in range (3):
            kehe.rt(120)
            basic2(kehe,x)
snowflake (bob,x = 10)    

bob.pu()
bob.lt(120)
bob.fd(50)
bob.pd()
"""
第三小题（我画的是书上的那个图形）
"""
def picture (kehe,x):#每一个由上一个曲线所组成的曲线都可以看作是组成下一个曲线的基本单位，与题2同理
    def base2 (kehe,x):#通过一级一级的调用，最终完成图形
        koch(kehe,x)
        kehe.lt(60)
        koch(kehe,x)
        kehe.rt(120)
        koch(kehe,x)
        kehe.lt(60)
        koch(kehe,x)
    def base3 (kehe,x):
        base2 (kehe,x)
        kehe.lt(60)
        base2 (kehe,x)
        kehe.rt(120)
        base2 (kehe,x)
        kehe.lt(60)
        base2 (kehe,x)
    def base4 (kehe,x):
        base3 (kehe,x)
        kehe.lt(60)
        base3 (kehe,x)
        kehe.rt(120)
        base3 (kehe,x)
        kehe.lt(60)
        base3 (kehe,x)
    def base5 (kehe,x):#最终图形
        base4 (kehe,x)
        kehe.lt(60)
        base4 (kehe,x)
        kehe.rt(120)
        base4 (kehe,x)
        kehe.lt(60)
        base4 (kehe,x)    
    base5(kehe,x)    
    turtle.mainloop()  
picture (bob,x = 5)    


    

