def A(m,n):
    if m==0:#满足第一种情况
        return n+1  
    elif m>0 and n==0:#满足第二种情况
        return A(m-1,1)
    elif m>0 and n>0:#满足第三种情况
        return A(m-1,A(m,n-1))
A(3,4)
