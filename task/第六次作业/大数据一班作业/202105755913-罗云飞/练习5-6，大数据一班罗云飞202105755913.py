import turtle
def koch(size,n):
    if n == 0:
        turtle.fd(size)
    else:
        for angle in [0,60,-120,60]:
            turtle.left(angle)
            koch(size/3,n-1)#定义科赫雪花
def main():
    turtle.setup(600,600)#建立一个绘图窗口
    turtle.penup()#提起画笔
    turtle.goto(-200,100)#乌龟移动到窗口上的位置
    turtle.pendown()#落下画笔
    turtle.pensize(3)#画笔的宽度
    turtle.speed(5)#画画的速度
    turtle.pencolor("blue")#蓝色画笔
    koch(400,3)#第一个曲线
    turtle.right(120)#乌龟右转
    turtle.pencolor("pink")#粉色画笔
    koch(400,3)#第二个曲线
    turtle.right(120)#乌龟继续右转
    turtle.pencolor("yellow")#黄色画笔
    koch(400,3)#第三个曲线
    turtle.done()#结束
main()

