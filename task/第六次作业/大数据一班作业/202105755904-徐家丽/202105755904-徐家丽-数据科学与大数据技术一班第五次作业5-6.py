#姓名：徐家丽
#学号：202105755904
#班级：数据科学与大数据技术一班
import turtle
pen=turtle.Turtle()
pen.pu()
pen.goto(-150,150)
pen.pd()
turtle.delay(0)
def koch(t,x):
    if x<3:
        t.fd(x)
    else:
        koch(t,x/3)
        t.lt(60)
        koch(t,x/3)
        t.lt(-120)
        koch(t,x/3)
        t.lt(60)
        koch(t,x/3)
def snowflake(t,x):
    koch(t,x)
    t.lt(-120)
    koch(t,x)
    t.lt(-120)
    koch(t,x)
snowflake(pen,300)#画科赫雪花，单个边300长度
turtle.mainloop()