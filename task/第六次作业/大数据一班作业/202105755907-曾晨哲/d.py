import turtle
bob = turtle.Turtle()

def koch(t, x):
    
    if x < 10:
        t.fd(x)
        return
    m = x/3
    koch(t, m)
    t.lt(60)
    koch(t, m)
    t.rt(120)
    koch(t, m)
    t.lt(60)
    koch(t, m)


def snowflake(t, x):
    
    for i in range(3):
        koch(t, x)
        t.rt(120)




bob.pu()
bob.goto(-150,90)
bob.pd()
snowflake(bob, 300)

turtle.mainloop()
        
