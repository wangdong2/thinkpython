import turtle
bob = turtle.Turtle()
def koch(t, length):
    if length<=10:
        t.fd(length)
    else:
        koch(t, length/3)
        t.lt(60)
        koch(t, length/3)
        t.rt(120)
        koch(t, length/3)
        t.lt(60)
        koch(t, length/3)
        
koch(bob,400)