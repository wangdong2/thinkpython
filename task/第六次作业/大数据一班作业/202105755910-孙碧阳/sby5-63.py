import turtle

bob = turtle.Turtle()

def koch(n,length):

    if(n==0):

        bob.forward(length)

    elif(n==1):

        bob.forward(length/3)

        bob.left(60)

        bob.forward(length/3)

        bob.right(120)

        bob.forward(length/3)

        bob.left(60)

        bob.forward(length/3)

    else:

        koch(n-1,length/3)

        bob.left(60)

        koch(n-1,length/3)

        bob.right(120)

        koch(n-1,length/3)

        bob.left(60)

        koch(n-1,length/3)

koch(4,400) 