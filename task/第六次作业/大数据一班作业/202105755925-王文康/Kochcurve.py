#202105755925
#大数据一班
#王文康
import turtle
bob=turtle.Turtle()
turtle.delay(0.01)
def koch(t, n):
    if n < 10:
        t.fd(n)
        return
    koch(t, n/3)
    t.lt(60)
    koch(t, n/3)
    t.rt(120)
    koch(t, n/3)
    t.lt(60)
    koch(t, n/3)
def snowflake(t, n):
    for i in range(3):
        koch(t, n)
        t.rt(120)
bob.lt(90)
bob.pu()
bob.fd(100)
bob.lt(90)
bob.fd(200)
bob.lt(180)
bob.pd()
snowflake(bob, 300)


turtle.mainloop()
    
