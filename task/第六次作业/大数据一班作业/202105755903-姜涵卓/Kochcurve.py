##班级：大数据（1）班  姓名：姜涵卓  学号：202105755903
#练习5-6
import turtle
bob=turtle.Turtle()
print(bob)
def koch(t,l ): ##t表示turtle，l表示曲线长度
    if l<3:
        t.fd(l)
    else:
        koch(t,l/3)
        t.lt(60) 
        koch(t,l/3)
        t.rt(120)
        koch(t,l/3)
        t.lt(60)
        koch(t,l/3)
##koch(t,l)函数表示一定长度科赫曲线


def snowflake(t,l):  ##t表示turtle，l表示曲线长度
    for i in range(3):
        koch(t,l) 
        t.rt(120)  ##每两条科赫曲线交界处转120°
##snowflake(t,l)由三条科赫曲线组成
snowflake(bob,100)
turtle.mainloop()