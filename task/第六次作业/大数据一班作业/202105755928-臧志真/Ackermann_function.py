def ackermann_A(m,n):
    if m == 0:
        return n + 1
    elif m > 0 and n == 0:
        return A(m-1,1)
    elif m > 0 and n > 0:
        return ackermann_A(m - 1,ackermann_A(m,n - 1))
ackermann_A(3,4)
"""
如果m和n过大，例如输入 ackermann_A(3,56) ,就会出现如下情况
---------------------------------------------------------------------------
RecursionError                            Traceback (most recent call last)
<ipython-input-10-3193270070ab> in <module>
----> 1 A(3,56)

<ipython-input-9-e378ac57f02b> in A(m, n)
      5         return A(m-1,1)
      6     elif m > 0 and n > 0:
----> 7         return A(m - 1,A(m,n - 1))
      8 A(3,4)

... last 1 frames repeated, from the frame below ...

<ipython-input-9-e378ac57f02b> in A(m, n)
      5         return A(m-1,1)
      6     elif m > 0 and n > 0:
----> 7         return A(m - 1,A(m,n - 1))
      8 A(3,4)

RecursionError: maximum recursion depth exceeded in comparison
"""