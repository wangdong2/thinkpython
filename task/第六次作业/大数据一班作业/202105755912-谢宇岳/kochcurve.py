# 谢宇岳
# 大数据一班
# 202105755912

# 第一问
import turtle
bob=turtle.Turtle()

def koch(t,m):
    # 画一定长度的科赫曲线
    if m>=10 :
        koch(t, m/3)
        t.lt(60)
        koch(t, m/3)
        t.rt(120)
        koch(t, m/3)
        t.lt(60)
        koch(t, m/3)
    else:
        t.fd(m)
        
koch(bob,10)
turtle.mainloop()

# 第二问

def snowflake(t,m):
    for i in range(3):
        koch(t,m)
        t.rt(120)

snowflake(bob, 300)

# 第三问


    
