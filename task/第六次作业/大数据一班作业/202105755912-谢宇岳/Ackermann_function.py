# 谢宇岳
# 大数据一班
# 202105755912

#练习6-2
def ack(m,n):
    if m==0:
        return n+1
    if m>0 and n==0:
        return ack(m-1,1)
    if m>0 and n>0:
        return ack(m-1,ack(m,n-1))
ack(3,4)
对于很大的数字，ack函数会无限递归，没有运算结果
