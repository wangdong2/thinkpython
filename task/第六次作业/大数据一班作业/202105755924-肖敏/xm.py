练习6-2
def ackermann(m,n):
    if m == 0:
        return n+1
    elif n == 0:
        return ackermann(m-1, 1)
    return ackermann(m-1, ackermann(m, n-1))


print(ackermann(3, 4))

