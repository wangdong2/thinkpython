练习5-6
答：
import turtle #引入乌龟模块
bob=turtle.Turtle()
bob.delya=0.01

def koch(t,length): #科赫曲线
    if length<=30:
        t.fd(length)
    else:
        koch(t,length/3)
        t.rt(60)
        koch(t,length/3)
        t.lt(120)
        koch(t,length/3)
        t.rt(60)
        koch(t,length/3)
        
def snowflake(t,length): #雪花
    koch(t,length)
    t.rt(120)
    koch(t,length)
    t.rt(120)
    koch(t,length)
    t.rt(120)
    
snowflake(bob,900)
   
