import turtle
def draw(t,x,n):
    m=x/3
    if n==0:
        return
    else:
        t.fd(m)
        return
    draw(t,x,n)
    t.lt(60)
    draw(t,x,n)
    t.rt(120)
    draw(t,x,n)
    t.lt(60)
    draw(t,x,n-1)

bob=turtle.Turtle()
draw(bob,30,20)
turtle.m()
