import turtle
bob = turtle.Turtle()
def koch(t, length):
    if length <= 20:
        t.fd(length)
    else:
        koch(t, length/3)
        t.lt(60)
        koch(t, length/3)
        t.rt(120)
        koch(t, length/3)
        t.lt(60)
        koch(t, length/3)

def snowflake(t, length):
    for i in range(3):
        koch(t, length)
        t.rt(120)

snowflake(bob, 300)