import turtle
bob=turtle.Turtle()
def move(t,length): # 画笔的提笔与落笔
    t.pu() # 提笔
    t.fd(length) # 向前走length
    t.pd() # 落笔
move(bob,-200) # 向后走200

def koch(t,x): # 定义一条科赫曲线
    if x<3:
        t.fd(x) # 向前走x
    else:
        x2=x/3 # 递归
        koch(t,x2) # x变为原来x的三分之一，直至有意义
        t.lt(60)
        koch(t,x2) # 包含于上一个koch
        t.rt(120)
        koch(t,x2)
        t.lt(60)
        koch(t,x2)
koch(bob,200)  # 第一问的一条科赫曲线      
def move2(t,length2):# 再一次提笔落笔
    t.pu()
    t.rt(90) # 向右转 ，为了使第一题曲线与第二题曲线不重合
    t.fd(length2) 
    t.pd()
move2(bob,100)
def m(t,x): # 三条科赫曲线
    koch(t,x) # 调用koch
    t.rt(120) # 右转连接两条科赫曲线
    koch(t,x) 
    t.rt(120)
    koch(t.x)
m(bob,100) # 第二问的三条科赫曲线
turtle.mainloop()




