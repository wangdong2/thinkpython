#202105756021  大数据2班  康丹琪
 #1.练习5-6科赫曲线
import turtle
bob=turtle.Turtle()
bob.delay=0.000000000000000000000000000000000000000000001


def koch(t,x):#x=length
    if x<6:
        t.fd(x)
        return
    r=x/3
    koch(t,r)
    t.lt(60)
    koch(t,r)
    t.rt(120)
    koch(t,r)
    t.lt(60)
    koch(t,r)
    
def snowflake(t,x):#画一朵雪花
    for i in range(3):
        koch(t,x)
        t.rt(120)
        
        
bob.color('blue','pink')
bob.pu()
bob.goto(-150,90)
bob.pd()
snowflake(bob,300)
turtle.mainloop()
    
    
    
