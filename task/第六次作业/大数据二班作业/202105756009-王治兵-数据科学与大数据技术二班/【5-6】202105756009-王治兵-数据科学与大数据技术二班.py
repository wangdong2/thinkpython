import turtle

turtle.Turtle().screen.delay(0)
bob = turtle.Turtle()

def koch(t,x):
    if x < 10:
        t.fd(x)
    else:
        n = x/3
        koch(t,n)
        t.lt(60)
        koch(t,n)
        t.rt(120)
        koch(t,n)
        t.lt(60)
        koch(t,n)
    
def snowflake(t,n,s):
    for i in range(s):
        koch(t,n)
        angle = 360/s
        t.rt(angle)
        
snowflake(bob,300,3)
turtle.mainloop()