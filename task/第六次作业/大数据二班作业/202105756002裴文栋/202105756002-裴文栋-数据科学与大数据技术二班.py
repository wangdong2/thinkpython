import turtle


def koch( turtle,x):
    if x<=3:
        turtle.fd(x)
        return
    
    n=x/3
    koch(turtle,n)
    turtle.lt(60)
    koch(turtle,n)
    turtle.rt(120)
    koch(turtle,n)
    turtle.lt(60)
    koch(turtle,n)
    
turtle.delay(0)

turtle.pu()

turtle.fd(-100)

turtle.pd()
for i in range(3):
    koch(turtle,300)
    turtle.rt(120)

turtle.mainloop()

