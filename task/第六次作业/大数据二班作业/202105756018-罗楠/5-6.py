import turtle
bob=turtle.Turtle()
def koch(t,x):
    if x<10:
        t.fd(x)
        return
    m=x/3
    koch(t,m)
    t.lt(60)
    koch(t,m)
    t.rt(120)
    koch(t,m)
    t.lt(60)
    koch(t,m)
def snowflake(t,x):
    for i in range(x):
        koch(t,x)
        t.rt(120)

snowflake(bob,100)
turtle.mainloop()

