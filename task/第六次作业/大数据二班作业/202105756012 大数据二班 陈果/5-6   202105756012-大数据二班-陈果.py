"""
学号：202105756012
班级:大数据二班
姓名：陈果

"""
import turtle as t
def koch(t, n):
    if n < 10:
        t.fd(n)
        return
    m = n/3
    koch(t, m)
    t.lt(60)
    koch(t, m)
    t.rt(120)
    koch(t, m)
    t.lt(60)
    koch(t, m)
def snowflake(t, n):
    for i in range(3):
        koch(t, n)
        t.rt(120)

t.speed(0)
t.pu()
t.bk(160)
t.pd()
snowflake(t, 270)
t.hideturtle()
t.mainloop()
