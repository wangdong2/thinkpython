def ack(m, n):
    A = n + 1
    if m == 0:
        return A
    if m > 0 and n == 0:
        return ack(m - 1, 1)
    if m > 0 and n > 0:
        return ack(m - 1, ack(m, n - 1))
        
ack(3, 4)
