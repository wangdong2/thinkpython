import turtle
alan=turtle.Turtle()
def koch(t,n):
    if n<10:
        t.fd(n)
        return#当n<10,return语句运行，当前函数终结
    m=n/3
    koch(t,m)
    t.lt(60)
    koch(t,m)
    t.rt(120)
    koch(t,m)
    t.lt(60)
    koch(t,m)
def snowflake(t,n):
    for i in range(n):
        koch(t,n)
        t.rt(120)
alan.pu()
alan.goto(-300,90)
alan.pd()

snowflake(alan,90)
turtle.mainloop()