#p52 练习5-2

"""
绘制科赫曲线
"""
import turtle
def koch(size,n): #size表示一条直线的长度
    if n==0:
        turtle.fd(size)
    else:
        for angle in[0,60,-120,60]:
            turtle.lt(angle)
            koch(size/3,n-1) #运用递归方式绘制科赫曲线
def main(): #定义主函数
    turtle.setup(800,400)
    turtle.penup()
    turtle.goto(-300,50)
    turtle.pendown()
    turtle.pensize(2)
    koch(600,3)
    turtle.hideturtle()
main()
    
 
"""
绘制科赫雪花
"""
import turtle
def koch(size,n):
    if n==0:
        turtle.fd(size)
    else:
        for angle in[0,60,-120,60]:
            turtle.lt(angle)
            koch(size/3,n-1)
def main(): #定义主函数
    turtle.setup(600,600)
    turtle.penup()
    turtle.goto(-200,100)
    turtle.pendown()
    turtle.pensize(2)
    level=3 #绘制三阶科赫雪花
    koch(400,level)
    turtle.rt(120)
    koch(400,level)
    turtle.rt(120)
    koch(400,level)
    turtle.hideturtle()
main()




    
       