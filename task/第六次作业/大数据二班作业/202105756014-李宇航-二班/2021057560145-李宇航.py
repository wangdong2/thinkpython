#*学号：202105756014
#*姓名：李宇航
#*班级：数据科学与大数据技术二班
import turtle 
bob=turtle.Turtle()
print(bob)
def koch(bob, n):
    if n < 10:
        bob.fd(n)
        return
    bob.speed(0)
    m = n/3
    koch(bob, m)
    bob.lt(60)
    koch(bob, m)
    bob.rt(120)
    koch(bob, m)
    bob.lt(60)
    koch(bob, m)
def snowflake(bob, n):
    for i in range(3):
        koch(bob, n)
        bob.rt(120)       
bob.pu()
bob.bk(160)
bob.pd()
snowflake(bob, 270)
bob.hideturtle()
turtle.mainloop()
