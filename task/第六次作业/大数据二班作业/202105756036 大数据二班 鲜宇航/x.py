import turtle
t=turtle.Turtle()
def move(t,length):
    t.pu()
    t.fd(length)
    t.pd()
    
def part(n):
    t.fd(n/3)
    t.lt(60)
    t.fd(n/3)
    t.rt(120)
    t.fd(n/3)
    t.lt(60)
    t.fd(n/3)
    
    
def a(n):
    part(n)
    t.lt(60)
    part(n)
    t.rt(120)
    part(n)
    t.lt(60)
    part(n)
def b(n):
    a(n)
    t.lt(60)
    a(n)
    t.rt(120)
    a(n)
    t.lt(60)
    a(n)
def c(n):
    b(n)
    t.lt(60)
    b(n)
    t.rt(120)
    b(n)
    t.lt(60)
    b(n)
def koch(n):
    c(n)
    t.lt(60)
    c(n)
    t.rt(120)
    c(n)
    t.lt(60)
    c(n)
    
    
    
move(t,-200)
koch(5)
        
    

turtle.mainloop()
