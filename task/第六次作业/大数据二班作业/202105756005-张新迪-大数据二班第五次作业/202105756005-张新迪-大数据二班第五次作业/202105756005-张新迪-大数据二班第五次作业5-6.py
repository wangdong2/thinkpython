#姓名：张新迪
#学号：202105756005
#班级：数据科学与大数据技术二班
import turtle
t=turtle.Turtle()
turtle.delay(0)
t.pu()
t.goto(-250,150)
t.pd()
def koch(t,x):
    '''
    画一条科赫曲线
    t:Turtle
    x:曲线长度
    '''
    if x<3:
        t.fd(x)
    else:
        for i in [0,60,-120,60]:
            t.lt(i)
            koch(t,x/3)
def snowflake(t,x):
    '''
    画三条科赫曲线并组成一个雪花形状
    t:Turtle
    x;单个科赫曲线长度
    '''
    koch(t,x)
    t.rt(120)
    koch(t,x)
    t.rt(120)
    koch(t,x)
    t.hideturtle()
    turtle.done()
snowflake(t,500)#这里演示的是三条长度均为500的科赫曲线