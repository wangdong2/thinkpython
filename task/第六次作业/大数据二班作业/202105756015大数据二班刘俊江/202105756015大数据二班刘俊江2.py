#5-6作业
#第一小问，科赫曲线
import turtle as tl
tl.screensize(1000,800)#为了让科赫函数应用的更彻底，我要把length设的长一点，所以我把画布大小改的更大，更好看
def koch(t, length):#定义函数
    """Draws a koch curve with length n."""
    if length < 10:#设置停止递归的条件，相当于设置最短的一段线条要小于10，最后每一条线段的长度都是length/3**n，n未知，但是最length/3**（n-1）>10。
        t.fd(length)#这个就是我们的主要函数，就是一条直线，科赫曲线就是4n条直线的组成
        return#返回，不再递归
    m = length/3#数学上分析科赫函数后明白科赫函数本是一条线段，但是不断重复把线段三等分，再组成一个角的样子。所以我们设置递归的下一个函数的length为m
    koch(t, m)#函数递归，其实函数在这里一直循环length，直到m<10，才开始画图
    t.lt(60)#转换角度
    koch(t, m)#这个函数与下面的其实都是一样的
    t.rt(120)#转换角度
    koch(t, m)
    t.lt(60)#转换角度
    koch(t, m)
tl.penup()#改变画笔的位置，花第一题的图
tl.lt(180)
tl.fd(500)
tl.lt(270)
tl.fd(300)
tl.rt(90)
tl.pendown()
koch(tl,300)
#第二小问，雪花
def snowflake(t,length,n):#定义函数，雪花（snowflake），其实雪花原本是一个三角形，也是像科赫曲线那样的转换，所以我可以用上面的koch函数
    if n==0:#设置停止递归的条件，设置了这个条件就是循环n次，我们也可以用for in来实现。
        return
    koch(t,length)#t调用函数
    tl.rt(120)#因为是三角形，所以转换的角度是360/3=120
    snowflake(t,length,n-1)#递归
tl.penup()#改变画笔位置，防止重叠
tl.rt(90)
tl.fd(100)
tl.rt(90)
tl.fd(200)
tl.lt(180)
tl.pendown()
snowflake(tl,300,3)#调用函数
tl.penup()#改变画笔位置，防止重叠
tl.fd(400)
tl.pendown()
#第三小问，泛化，进那个网站可累坏了，太难进了
#实现网站中的泛化只需要修改length的大小，如length小于10的话会直接就是一条直线来组成雪花，就是三角形，10<length<30的话就是一个六芒星，以此类推
#我在下面演示一下
snowflake(tl,9,3)
tl.penup()#改变画笔位置，防止重叠
tl.fd(50)
tl.pendown()
snowflake(tl,29,3)
tl.mainloop()
#6-2作业
def ack(m,n):#首先定义函数
    if m==0:#设置返回条件
        return n+1#返回
    elif m>0 and n==0:#elif设置另一个返回条件
        return ack(m-1,1)#返回
    elif m>0 and n>0:#同上
        return ack(m-1,ack(m,n-1))#返回
ack(3,4)#调用
#这个作业就非常简单了，就是条件判断和返回的运用，代码部分3分钟吧。
