"""
202105756035
大数据二班
胡佳明
"""
import turtle as t

def Koch_curve(t,x):
    if x<10:
        t.fd(x)
        return
    y=x/3
    t.speed(0)
    Koch_curve(t,y)
    t.lt(60)
    Koch_curve(t,y)
    t.rt(120)
    Koch_curve(t,y)
    t.lt(60)
    Koch_curve(t,y)


t.pu()
t.fd(-150)
t.pd()
Koch_curve(t,300)
t.rt(120)
Koch_curve(t,300)
t.rt(120)
Koch_curve(t,300)

    
t.mainloop()