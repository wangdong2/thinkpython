#kochDrawV1;.py
import turtle
def koch(size,n):#size表示绘制科赫曲线的每一个直线的长度，n表示绘制的阶数
    if n==0:
        turtle.fd(size)#绘制一条直线
    else:
        for angle in[0,60,-120,60]:
            turtle.left(angle)
            koch(size/3,n-1)#利用递归方式绘制
def main():#定义主的控制过程
    turtle.setup(800,400)#定义turtle窗体大小
    turtle.penup()
    turtle.goto(-300,-50)
    turtle.pendown()
    turtle.pensize(2)#画笔宽度
    koch(600,3)#调用函数绘制长度为600的像素，阶数为3的科赫曲线
    turtle.hideturtle()
main()