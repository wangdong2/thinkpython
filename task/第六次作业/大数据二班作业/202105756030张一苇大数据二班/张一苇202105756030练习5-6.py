'''
1.编写一个函数koch,接收一个Turtle以及一个长度作为形参，并用Turtle绘制指定长度的科赫曲线。
'''
import turtle
bob = turtle.Turtle()
bob.delay = 0.0001
def koch(t,length):                   #函数koch, 以Turtle和长度作为形参
    if length<=3:                   #当长度小于3时，绘制成直线
        t.fd(length)
    else:
        koch(t,length/3)
        t.lt(60)
        koch(t,length/3)
        t.rt(120)
        koch(t,length/3)
        t.lt(60)
        koch(t,length/3)
#koch(bob,x)     绘制长度为x的科赫曲线



'''
2.编写一个函数snowflake,绘制3条科赫曲线，组成一个雪花形状。
'''
def snowflake(t,length):         #函数snowflake，以Turtle和长度作为形参
    angle=120               #旋转360/3°
    koch(t,length)
    t.rt(angle)
    koch(t,length)
    t.rt(angle)
    koch(t,length)
#snowflake(bob,x)    #绘制长度为x的雪花     



'''
3.科赫曲线可以用几种方法泛化。
'''
import turtle
def kehe(size,n):
    if n==0:
        turtle.fd(size)
    else:
        for angle in [0,60,-120,60]:
            turtle.left(angle)
            kehe(size/3,n-1)

#kehe(1200,6)
#6阶科赫曲线
