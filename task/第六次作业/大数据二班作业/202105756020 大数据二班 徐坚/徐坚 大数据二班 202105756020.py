"""
徐坚
大数据二班
202105756020
"""
import turtle
bob = turtle.Turtle()

def koch(t, n):
    if n<=3:
        t.fd(n)
    else:
        koch(t, n/3)   
        t.lt(60)
        koch(t, n/3)
        t.rt(120)
        koch(t, n/3)
        t.lt(60)
        koch(t, n/3)

def snowflake(t, length):
    koch(t, length)
    t.rt(120)
    koch(t, length)
    t.rt(120)
    koch(t, length)
    t.rt(120)

snowflake(bob, 300)
bob.ht()
turtle.mainloop()

