'''
班级：大数据二班
姓名：丛博予
学号：202105756027
练习5-6
'''
import turtle as t

def koch(t,x):#建立一个科赫曲线函数
    if x<=5:
        t.fd(x)
        return
    else:
        n=x/3
        koch(t,n)
        t.lt(60)
        koch(t,n)
        t.rt(120)
        koch(t,n)
        t.lt(60)
        koch(t,n)
def snowflake(t,x):#建立雪花函数
    for i in range(3):
        koch(t,x)
        t.rt(120)
    
t.speed(0)#设置速度
t.penup()#确定落点
t.fd(-300)
t.lt(90)
t.fd(150)
t.rt(90)
t.pendown()
snowflake(t,500)#运行函数
t.mainloop()
