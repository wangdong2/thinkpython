import turtle
bob=turtle.Turtle()
def move(t,length):
    t.pu()
    t.fd(length)
    t.pd()
    
def part(n):
    t.fd(n/3)
    t.lt(60)
    t.fd(n/3)
    t.rt(120)
    t.fd(n/3)
    t.lt(60)
    t.fd(n/3)
    
    
def kochone(n):#第一个科赫曲线
    part(n)
    t.lt(60)
    part(n)
    t.rt(120)
    part(n)
    t.lt(60)
    part(n)
def kochtwo(n):#第二个科赫曲线
    kochone(n)
    t.lt(60)
    kochone(n)
    t.rt(120)
    kochone(n)
    t.lt(60)
    kochone(n)
def kochthree(n):#第三个科赫曲线
    kochtwo(n)
    t.lt(60)
    kochtwo(n)
    t.rt(120)
    kochtwo(n)
    t.lt(60)
    kochtwo(n)
def koch(n):#整合
    kochthree(n)
    t.lt(60)
    kochthree(n)
    t.rt(120)
    kochthree(n)
    t.lt(60)
    kochthree(n)
    
    
    
move(bob,-200)
koch(5)
        
    

turtle.mainloop()