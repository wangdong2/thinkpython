"""

丁宇宏 大数据二班 202105756013
练习 5-6：科赫曲线
"""


import turtle


bob = turtle.Turtle()
turtle.delay(0.01)
bob.color("green")


def koch(t, n):
    if n<=3:
        t.fd(n)
    else:
        koch(t, n/3)   #调用自己直到其<30为止 ，
        t.lt(60)
        koch(t, n/3)
        t.rt(120)
        koch(t, n/3)
        t.lt(60)
        koch(t, n/3)

def snowflake(t, length):
    koch(t, length)
    t.rt(120)
    koch(t, length)
    t.rt(120)
    koch(t, length)
    t.rt(120)

snowflake(bob, 300)
bob.ht()
turtle.mainloop()

