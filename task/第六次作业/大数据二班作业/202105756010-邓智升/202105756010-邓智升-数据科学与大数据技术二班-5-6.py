import turtle
turtle.Turtle().screen.delay(0)
bob=turtle.Turtle()
print(bob)
def move(t, length):
    t.pu()
    t.fd(length)
    t.pd()
#1.
def koch(length):
    if length<3:
        bob.fd(length)
    else:
        koch(length/3)
        bob.lt(60)
        koch(length/3)
        bob.rt(120)
        koch(length/3)
        bob.lt(60)
        koch(length/3)
        
move(bob,-350)        
koch(300)

#2.
def snowflake(x):
    for i in range(3):
        koch(x)
        bob.rt(120)

move(bob,100)
snowflake(300)

def L(x,n):
    for i in range(n):
        koch(x)
        bob.lt(360/n)
L(100,6)
turtle.mainloop()