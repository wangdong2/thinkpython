#姓名：刘烨
#班级：数据科学与大数据技术2班
#学号：202105756034
def ack(m,n):
    if m==0:
        return n+1
    if m>0 and n==0:
        return ack(m-1,1)
    if m>0 and n>0:
        return ack(m-1,ack(m,n-1))
print(ack(3,4))
###
#如果m,n的数字很大会报错
###