import turtle
t=turtle.Turtle()
def koch(t,n):
    if n < 3 :
        t.fd(n)
        return
    m = n/3
    koch(t,m)
    t.lt(60)
    koch(t,m)
    t.rt(120)
    koch(t,m)
    t.lt(60)
    koch(t,m)
    
def snowflake(t,n):
    for i in range(3):
        koch(t,n)
        t.rt(120)

def move(t,length):
    t.pu()
    t.fd(length)
    t.pd()

move(t,-300)
snowflake(t,240)#调用函数开始画雪花
move(t,350)
koch(t,240)
turtle.mainloop()
 
