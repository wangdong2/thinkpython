#学号:202105756017
#姓名:龙玉祥
#班级:大数据二班
import turtle
t=turtle.Turtle()
def koch(t,x):
    if x<3:
        t.fd(x)
    else:
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)
        t.rt(120)
        t.fd(x/3)
        t.lt(60)
        t.fd(x/3)
def snowflake(t,x):
    for i in range(3):
        koch(t,x)
        t.rt(120)
snowflake(t,100)
turtle.mainloop()