import turtle
bob=turtle.Turtle()
def koch_0(t,x):#y为1时的函数，x为科赫曲线的长度。注：当y比较大时，x要非常大曲线才明显
    t.fd(x/3)
    t.lt(60)
    t.fd(x/3)
    t.rt(120)
    t.fd(x/3)
    t.lt(60)
    t.fd(x/3)
def koch(t,x,y):#主函数
    if y==0:
        t.fd(x)
    elif y==1:
        koch_0(t,x)#副函数
    else:
        koch(t,x/(3*4**(y-1)),y-1)
        t.lt(60)
        koch(t,x/(3*4**(y-1)),y-1)
        t.rt(120)
        koch(t,x/(3*4**(y-1)),y-1)
        t.lt(60)
        koch(t,x/(3*4**(y-1)),y-1)
def snowflake(t,x,y):#科赫雪花
    koch(t,x,y)
    t.lt(120)
    koch(t,x,y)
    t.lt(120)
    koch(t,x,y)
def ack(m,n):
    if m==0:
        return n+1
    elif m>0 and n==0:
        return ack(m-1,1)
    elif m>0 and n>0:
        return ack(m-1,ack(m,n-1))