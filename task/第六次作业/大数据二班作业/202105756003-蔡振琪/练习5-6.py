import turtle
bob=turtle.Turtle()
def koch(t, x):     #t为乌龟对象，x为曲线总长
    if x < 10:
        t.fd(x)
        return
    length= x/3    #length为分形长
    koch(t, length)
    t.lt(60)
    koch(t, length)
    t.rt(120)
    koch(t, length)
    t.lt(60)
    koch(t, length)
def snowflake(m,n):   #m为乌龟对象，n为一段曲线总长
    for i in range(3):
        koch(m,n)
        m.rt(120)
    return
def Koch_snowflake(a,b,c):#a为乌龟对象，b为一段曲线总长,c为曲线条数
    for j in range(c):
        koch(a,b)
        a.rt(360/c)
    return
Koch_snowflake(bob,300,3)
turtle.mainloop()
