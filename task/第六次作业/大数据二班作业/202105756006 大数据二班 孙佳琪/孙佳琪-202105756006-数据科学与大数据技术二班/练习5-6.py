import turtle

turtle.delay(0)

def move(t,length):#移动bob  
    t.pu()
    t.fd(length)
    t.pd()    
#先编写一条科赫曲线
def koch(t,x): 
    #定义一个函数 科赫曲线，完成绘画功能，其中x代表长度
    if x < 3:
        t.fd(x) 
        return
    s = x/3
    koch(t,s)
    t.lt(60)
    koch(t,s)
    t.rt(120)
    koch(t,s)
    t.lt(60)
    koch(t,s)
#1.编写如图5-2的科赫曲线
def kehe(x):#重复三条科赫曲线
    koch(turtle,x/3)
    turtle.lt(60)
    koch(turtle,x/3)
    turtle.rt(120)
    koch(turtle,x/3)
    turtle.lt(60)
    koch(turtle,x/3)
    turtle.hideturtle() 
#2.定义雪花曲线           
def snowflake(x):#重复三条科赫曲线
    koch(turtle,x/3)
    turtle.rt(120)
    koch(turtle,x/3)
    turtle.rt(120)
    koch(turtle,x/3)
    turtle.hideturtle() 
#3.泛化科赫曲线
def koch_second(turtle,x,n):#n代表阶数
    if n==0:
        turtle.fd(x)
    else:
        for angle in [0,60,-120,60]:
            turtle.lt(angle)
            koch_second(turtle,x/3,n-1)

#1.画如图5-2的科赫曲线
turtle.lt(90)    
move(turtle,200)
turtle.rt(90)
move(turtle,-700)
kehe(600)
#2.绘制雪花图案
move(turtle,200)
turtle.lt(90)   
move(turtle,100)
turtle.rt(90)
snowflake(600)
#3.利用泛化的科赫曲线绘制图像
#1)例如：画一个一阶科赫曲线
turtle.rt(210)
move(turtle,500)
turtle.lt(90)
move(turtle,-800)
koch_second(turtle,200,1)
#2）例如：画一个三阶科赫曲线
move(turtle,100)
koch_second(turtle,300,3)
#3）例如：画一个五阶科赫曲线
move(turtle,100)
koch_second(turtle,600,5)

turtle.mainloop()

