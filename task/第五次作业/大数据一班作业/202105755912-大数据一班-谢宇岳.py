# 谢宇岳 大数据一班 202105755912
import math
import turtle
bob = turtle.Turtle()
turtle.delay(0.01) 
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
        
def polygon(t,n,length):
    angle=360/n
    polyline(t,n,length,angle)
    
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n = int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
    
def huahua(t,d,angle):
    """
    弦长为d
    弦切角为angle
    """
    r = d / 2/math.sin(angle/2*math.pi/180)
    arc(t,r,angle)
    t.lt(180 - angle)
    arc(t,r,angle)
    t.lt(180 - angle)
    
def hua(t,n,m,r):
    """
    n为花瓣个数
    m为花瓣重叠层数
    r为圆弧半径长度
    """
    for i in range(n):
        huahua(t,r,360*m/n)
        t.lt(360/n)
        
def move(t,length): 
    t.pu()
    t.fd(length)
    t.pd()
    
move(bob,-200)
hua(bob,7,1,100)
move(bob,200)
hua(bob,10,2,100)
move(bob,200)
hua(bob,20,1,100)


bob.hideturtle() #隐藏箭头
turtle.mainloop()