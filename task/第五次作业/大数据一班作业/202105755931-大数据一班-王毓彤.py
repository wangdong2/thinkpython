
import turtle#引入turtle
import math#引入math
t=turtle.Turtle()#将turtle用t代替

def polyline(t,n,length,angle):#定义polyline
    for i in range(n):
        t.fd(length)
        t.lt(angle)
        
def polygon(t,n,length):#定义polygon
    angle=360/n
    polyline(t,n,step_length,step_angle)
    
def arc(t,r,angle):#定义arc
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=angle/n
    polyline(t,n,step_length,step_angle)

def circle(t,r):#定义circle
    s=2*math.pi*r
    length=s/n
    polygon(t,n,length)
    
t.goto(0,0)
def flower_1(t,n,l,angle):#定义花朵，n为花瓣数量，l为花心到每一朵花瓣尖上的距离，angle为每朵花瓣的中线夹角，下同
    a=angle*math.pi/360#将angle换算成弧度制，这样才可以在sin中应用，下同
    r=l/2/math.sin(a)#计算r，下同
    for i in range(n):#将每一个花瓣的绘制重复n次，下同
        arc(t,r,angle)#让turtle行走一个半径为r，角度为angle的圆弧，下同
        t.lt(180-angle)#让turtle转头继续下一步，下同
        arc(t,r,angle)#让转头后的turtle再次行走一个半径为r，角度为angle的圆弧，下同
        t.lt(180)#让turtle调头，绘制下一个相邻花瓣，下同
        
flower_1(t,7,50,360/7)#第一朵花
t.penup()#抬笔

t.goto(150,0)#让turtle去坐标位置开始下一朵花的绘制
t.pendown()#落笔
def flower_2(t,n,l,angle):#定义第二朵花，利用第一朵花的函数，让turtle先画一朵五瓣花，再回到起点，转一个角度（二分之一倍的两朵花瓣中线夹角），再画一遍五瓣花。
    a=angle*math.pi/360
    r=l/2/math.sin(a)
    for i in range(n):
        arc(t,r,angle)
        t.lt(180-angle)
        arc(t,r,angle)
        t.lt(180)
    t.penup()
    t.goto(150,0)
    t.pendown()
    t.lt(180/n)
    for i in range(n):
        arc(t,r,angle)
        t.lt(180-angle)
        arc(t,r,angle)
        t.lt(180)
flower_2(t,5,50,360/5)

t.penup()#提笔
t.goto(-150,0)#去往第三朵花的起始位置
t.pendown()#落笔
def flower_3(t,n,l,angle):#将第一朵花的函数改变参数大小来实现第三朵花。
    flower_1(t,n,l,angle)
flower_3(t,20,50,360/20)

t.hideturtle()
turtle.exitonclick()#让程序等待用户点击界面后才能退出。