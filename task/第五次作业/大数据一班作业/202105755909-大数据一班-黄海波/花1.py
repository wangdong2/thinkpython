import turtle
import math


bob = turtle.Turtle()


def xian(t, length, angle, n):
    for i in range(n):
        t.lt(angle)
        t.forward(length)
        
        
def kat(t, r, sa):  # 画一个叶瓣，sa为由一个正多边形画一个叶瓣所占角度占一圈的份数
    angle = 360 / sa
    arc_length = 2 * math.pi * r * angle / 360  # 一个半瓣的长度
    n = int(arc_length / 4) + 1
    st_length = arc_length / n
    st_angle = angle / n
    xian(t, st_length, st_angle, n)
    t.lt(180 - angle)
    xian(t, st_length, st_angle, n)
    t.lt(180)
    t.rt(angle/2)
    
def hua(t, r, sa):
    for k in range(sa*2):#sa*2为叶瓣数
        kat(t, r, sa)


hua(bob, 60, 5)

turtle.mainloop()