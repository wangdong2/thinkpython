"""
202105755919-大数据1班-刘一帆-练习四
"""
"""
第一小题
"""
#准备阶段
import time
import turtle
import math
flower = turtle.Turtle()
flower.speed(speed=0)
#函数：x个花瓣组成的花朵
def Complete_petal_1(x):
    for k in range (x):   
        for j in range(2):
            def petal(r ,angle):#花瓣的一半
                length = r * math.pi * 2 / 360
                for i in range(angle):
                    flower.fd(length)
                    flower.lt(1)
            petal(r = 10 * x ,angle = 360 // x)
            flower.lt(180 - 360 // x)
        flower.lt(360 // x)#保证括号内的数与angle = 360 // x 相同，因为执行完lt（360 // x）的命令后两条弧线刚好相切。
    
    
"""
第二小题
"""
def Complete_petal_2(x):
    for k in range (x):#由五个花瓣组成的花朵
        for j in range(2):
            def petal(r ,angle):
                length = r * math.pi * 2 / 360
                for i in range(angle):
                    flower.fd(length)
                    flower.lt(1)
            petal(r = 10 * x ,angle = 360 // x)
            flower.lt(180 - 360 // x)
        flower.lt(360 // x)
    flower.lt(35)#转向，使两个花朵以合适的角度
    for k in range (x):#仍然是五个花瓣组成的花朵   
        for j in range(2):
            def petal(r ,angle):
                length = r * math.pi * 2 / 360
                for i in range(angle):
                    flower.fd(length)
                    flower.lt(1)
            petal(r = 10 * x ,angle = 360 // x)
            flower.lt(180 - 360 // x)
        flower.lt(360 // x)
    
    

"""
第三小题
"""
def Complete_petal_3(x):#只需将第一小题的x = 7 改为 x = 12即可
    for k in range (x):   
        for j in range(2):
            def petal(r ,angle):
                length = r * math.pi * 2 / 360
                for i in range(angle):
                    flower.fd(length)
                    flower.lt(1)
            petal(r = 10 * x ,angle = 360 // x)
            flower.lt(180 - 360 // x)
        flower.lt(360 // x)
    turtle.mainloop()
Complete_petal_1 (x = 7)
flower.pu ()
flower.fd(150)
flower.bk
flower.pd ()
Complete_petal_2 (x = 5)
flower.pu ()
flower.fd(150)
flower.rt(35)
flower.pd ()
Complete_petal_3 (x = 20)    




   