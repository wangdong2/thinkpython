# 运行时提醒我.forward() missing 1 required positional argument: 'distance'
# 希望老师能讲下，找到了了一些，还是不太理解。
import turtle
bob = turtle.Turtle
from math import pi

def polyline(t,n,line,angle1,angle2):
    for i in range(n):
        t. fd(line)
        t. lt(angle1)
        t. lt(angle2)
        
def arc(t,r,angle):
    """
    t :turtle
    n: 花瓣个数
    step_line ；花瓣长度
    step_angle1: 花瓣弧度
    step_angle2: 花瓣尖
    """
    arc_length = 2*pi*r*angle/360
    n = int(arc_length/3)+1
    step_length = arc_length/n
    step_angle1 = float(angle)/n
    step_angle2 = 5*step_angle1
    print(step_length)
    return polyline(t, n, step_length, step_angle1, step_angle2)

def flower(t, n, r,angle):
    for i in range(n):
        arc(t,r,angle)

flower(bob, 6, 20, 120)
