#康文豪 大数据一班 202105755905
#第一幅图
import math
import turtle
bob=turtle.Turtle()
def arc(t,r,angle,m):
    arc_length=2*math.pi*r*angle/360
    n=50
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(m):
        for i in range(n):
            t.fd(step_length)
            t.lt(step_angle)
        t.lt(140)
        for i in range(n):
            t.fd(step_length)
            t.lt(step_angle)
        t.lt(90)
arc(bob,200,40,7)
turtle.mainloop()


