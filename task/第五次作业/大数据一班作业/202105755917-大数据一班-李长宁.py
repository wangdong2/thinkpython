import turtle
bob = turtle.Turtle()
print(bob)
import math
pi = math.pi
def polyline(t,n,length,angle):
    for i in range (0,n):
        t.fd(length)
        t.lt(angle)
def polygon(t,n,length):
    angle = 360/n
    polyline(t,n,length,angle)
def arc(t,r,angle):
    arc_length = 2*pi*r*angle/360
    n = int (arc_length/3)+1
    step_length = arc_length/n
    step_angle = float(angle)/n
    polyline(t,n,step_length,step_angle)
def petal(t, r, angle):
    for i in range(2):
        arc(t, r, angle)
        t.lt(180-angle)
def flower(t, n, r, angle):
    for i in range(n):
        petal(t, r, angle)
        t.lt(360.0/n)
def move(t, length):
    t.pu()
    t.fd(length)
#p1
move(bob, -100)
flower(bob, 7, 60.0, 60.0)
#p2
move(bob, 100)
flower(bob, 10, 40.0, 80.0)
#p3
move(bob, 100)
flower(bob, 20, 140.0, 20.0)
  t.pd()
turtle.mainloop()  
