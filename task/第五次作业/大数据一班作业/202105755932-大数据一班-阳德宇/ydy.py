#大数据一班 阳德宇 202105755932
import turtle
bob=turtle.Turtle()
import math

def arc(t,r,angle,m,y):
    arc_length=2*math.pi*r*angle/360
    n=50
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(m):        
        for i in range(n):
            t.fd(step_length)
            t.lt(step_angle)
        t.lt(180-angle)
        for i in range(n):        
            t.fd(step_length)
            t.lt(step_angle)
        t.lt(y)
bob.delay=0.01
arc(bob,500,22,20,140)
turtle.mainloop()