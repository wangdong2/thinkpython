import turtle

bob = turtle.Turtle()
bob.delay = 0.01

import math

def flower(t, r, e):
    length = 2*math.pi*r*e/360
    n = int(length/3)+1
    length_two = length/n
    angle = e/n
    for i in range(7):
         for j in range(n):
            t.bk(length_two)
            t.rt(angle)
         t.rt(180-e)
         for i in range(n):
                t.fd(length_two)
                t.rt(angle)
            
flower(bob,300,360/10)
print(bob)
bob.hideturtle()

turtle.mainloop()

    