
import turtle
import math
bob=turtle.Turtle()
def move(t,length): # 定义一个落笔与抬笔的函数，使三幅图画可以话在同一幅图中
    t.pu() # 抬笔
    t.fd(length) # 向前走
    t.pd() # 落笔
def ployline(t,n,length,angle): # 画弧的步骤，在arc函数中可直接调用
    for i in range(n): # 把每一小段重复n次
        t.fd(length) # 每一小段走的长度
        t.lt(angle) # 走过每一小段后左转的度数
def arc(t,r,angle): # 定义一个画弧的函数
    arc_length=2*math.pi*r*angle/360 # 弧长
    n=int(arc_length/3+1) # 弧长分为n小段
    step_length=arc_length/n # 每一小段走的度数
    step_angle=angle/n # 走过每一小段走过的度数
    ployline(t,n,step_length,step_angle)
def flower(t,x,y,r): # x为花瓣数 y为画交叉花瓣的次数 r为圆弧的半径
    angle=360/x # 每个花瓣所占的度数
    for j in range(x):
        arc(t,r,angle) # 画花瓣的其中一半
        t.lt(180-angle) # 调转画笔方向
        arc(t,r,angle) # 画花瓣的另一半
        t.rt(180) # 调转画笔方向
    t.lt(180/x) # 使画笔转半个花瓣的度数，可以交叉画花瓣
    for b in range(y): # 画y个交叉的花瓣
        arc(t,r,angle)
        t.lt(180-angle)
        arc(t,r,angle)
        t.rt(180)
flower(bob,7,0,50) # 画7个花瓣，圆弧半径为50的花   
move(bob,150) # 向前走150
flower(bob,5,5,50) # 画10个花瓣，（原本5个花瓣，交叉5个花瓣）圆弧半径为50的花
move(bob,-150) # 向后走150
flower(bob,20,0,150) # 画20个花瓣，圆弧半径为150的花
turtle.mainloop() 