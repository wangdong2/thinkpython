import turtle
import math
#使用书上弧线画法函数，这里我修改了一部分代码，用来实现控制花瓣长度不受弧线曲率影响
def polyline(t,n,length,angle1):
    for i in range(n):
        t.fd(length)
        t.lt(angle1)
def arc(t,s,angle):                             #弧线画法打包
    arc_length = 2 * math.pi *s/(2*math.sin(angle*math.pi/180)) * angle / 360
    #s对应了弧线对应的弦长，由数学知识可求得半径r=2*math.sin(angle*math.pi/180))
    n = int(arc_length/3) + 1
    step_length = arc_length/n
    step_angle = angle / n
    polyline(t,n,step_length,step_angle)
#花朵画法
def petal(t,s,angle):              #绘制花瓣petal
    for i in range(2):
        arc(t, s, angle) #弧线画法
        t.lt(180-angle)  #对称弧线形成花瓣
def flower(t,s,angle,m):           #循环花瓣
    for i in range(m):
        petal(t, s, angle)
        t.lt(360/m)                #每做完一片花瓣向左转的角度，直至走完一圈
s = int(input('花瓣长度（弦长长度）：'))
angle = int(input('花瓣宽度(弧对应角度 越大越宽）：'))         #已知拱高，半径求角度太难了
m = int(input('花瓣片数：'))
bob = turtle.Turtle()
flower(bob,s,angle,m)
turtle.mainloop()