##班级：大数据（1）班  姓名：姜涵卓  学号：202105755903
##练习4-2
import math
import turtle
bob=turtle.Turtle()
print(bob)
def arc(t,r,angle):##t表示turtle，r表示弧的半径，angle表示对弧的角度
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1 
    '''
    n表示花瓣数
    '''
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
    ##整个函数来表示圆弧

    
def petal(t,r,angle):##t表示turtle，r表示弧的半径，angle表示对弧的角度
    for j in range(2):
        arc(t,r,angle)
        t.lt(180-angle)  ##反向对弧
        ##整个函数表示一个花瓣
        

def flower(n,t,r,angle):  ##n表示花瓣数，t表示turtle，r表示弧的半径，angle表示对弧的角度
    for k in range(n):
        petal(t,r,angle)
        t.lt(360/n)
        
flower(7,bob,100,60)  ##第一个图案

flower(10,bob,100,80)  ##第二个图案

flower(20,bob,200,20)  ##第三个图案

turtle.mainloop()
        