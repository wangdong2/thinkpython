import turtle
import math
bob = turtle.Turtle()
def polygon(t,n,length):
    angle=360/n
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
def pa(t,r,angle):
    for i in range(2):
        arc(t,r,angle)
        t.lt(180-angle)
def flower(t, n, r, angle):
    for i in range(n): 
        pa(t, r, angle) 
        t.lt(360.0/n) 
def move(t, length):
    t.pu() 
    t.fd(length) 
    t.pd() 
move(bob, -100) 
flower(bob, 7, 60.0, 60.0) 
move(bob, 100) 
flower(bob, 10, 40.0, 80.0) 
move(bob, 100) 
flower(bob, 20, 140.0, 20.0) 
bob.hideturtle() 
turtle.mainloop() 
  
  
