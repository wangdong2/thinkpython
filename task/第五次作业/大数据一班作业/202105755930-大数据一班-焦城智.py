'''
大数据一班
焦城智
202105755930
4-2第一题
'''
import turtle
import math
bob=turtle.Turtle()
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
def flower(r,m,v):
    for i in range(m):#这是把一朵花重复m次
        arc(bob,r,v)
        bob.lt(180-v)
        arc(bob,r,v)
        bob.lt(180)
        bob.rt(v-360/m)
        bob.delay=0.01  #r为半径,m为花的朵数,v为圆心角度数，当v=360/m时画出的是1，3图像
#当v>360/m时画出的是重合图像
bob.pu()
bob.lt(180)
bob.fd(200)
bob.pd()
flower(100,7,360/7)
bob.pu()
bob.bk(200)
bob.pd()
flower(100,10,60)
bob.pu()
bob.bk(200)
bob.pd()
flower(400,20,360/20)
  
  


    