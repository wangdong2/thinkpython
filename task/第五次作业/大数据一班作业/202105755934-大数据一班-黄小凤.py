import math
import turtle
bob=turtle.Turtle()
bob.speed(0)##画笔的速度
bob.setheading(180)##置顶画笔起始方向：向西
bob.penup()##提起画笔
bob.fd(300)##提起画笔后移动300
bob.pendown()##画笔落下
def polyine(t,n,length,angle):
    """length:长度
    angle:角度
    """
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(bob,r,angle):
    """r:所画圆弧对应圆的半径
    angle:每段圆弧对应的角度
    """
    arc_length=2*math.pi*r*angle/360##根据角度和半径计算出每一段圆弧的长度
    n=int(arc_length/3)+1##计算画一段长度为arc_length的圆弧一共需要重复n次
    step_length=arc_length/n##计算每一次要画的长度
    step_angle=float(angle)/n##计算每一次偏转的角度
    polyine(bob,n,step_length,step_angle)
def single_make(bob,r,m):
    """r:对应圆弧的半径
    m:单层花瓣的瓣数
    """
    angle=360/m ##每段圆弧对应的角度 
    arc(bob,r,angle)##画一片花瓣中的第一段圆弧
    bob.lt(180-angle)##画完第一段圆弧后偏转的角度
    arc(bob,r,angle)##画一片花瓣中的第二段圆弧
    bob.lt(180-2*angle)##画完第二段圆弧后的偏转角度，为画第二片花瓣的第一段圆弧做准备
def single_flower_make(bob,q,r,m): 
    """q:花朵的花瓣层数
    r:每一段圆弧对应圆的半径
    m:单层花瓣的瓣数
    """
    for j in range(m):##将一片花瓣重复m次
        angle=360/m##每段圆弧对应的角度
        single_make(bob,r,m)
    bob.lt(180-(q+1-m%2)*angle/q)##画完一层花瓣后旋转的角度，为画下一层花瓣做准备
def flower_make(bob,q,r,m):
    """q:花朵花瓣的层数
    r:每段圆弧对应圆的半径
    m:单层花瓣的瓣数
    """
    for k in range(q):##将一层花瓣重复再画q次
        single_flower_make(bob,q,r,m)  
flower_make(bob,1,150,7)##4—1（1）花朵
bob.setheading(0)##重置画笔方向，向东
bob.penup()##提起画笔
bob.fd(300)##提起画笔后移动300
bob.pendown()##落下画笔
flower_make(bob,2,100,5)##4—1（2）花朵
bob.setheading(0)##重置画笔方向，向东
bob.penup()##提起画笔
bob.fd(300)##提起画笔后移动600
bob.pendown()##落下画笔
flower_make(bob,1,400,20)##4—1(3)花朵
turtle.mainloop()