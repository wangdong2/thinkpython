import turtle#导入乌龟模块
import math#导入数学模块

def arc(t,arc_angle,r):
    ci = 2*math.pi*r*arc_angle/360.0
    length=2
    n=int(ci/length)+1
    length=ci/n
    angle=arc_angle/n
    polyline(t,n,angle,length)
    
def polyline(t,n,angle,length):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
       
    
    
def flower(t,angle,n,length):
    pental_angle=360.0/n
    for i in range (n):
        arc(t, angle,length)
        t.lt(180-angle)
        arc(t,angle,length)
        t.lt(180-angle)
        t.lt(pental_angle)
        
bob=turtle.Turtle()
  
#不重叠的七瓣花
flower(bob,60,7,150)

#不重叠的十瓣花
flower(b,75,10,150)

#不重叠的二十瓣花
flower(bob,30,20,300)

