import turtle
bob=turtle.Turtle()
print(bob)
import math
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)

n=7
def circle(t,r):
    arc(t,r,360/n)
for i in range(n):
    circle(bob,100)
    bob.lt(180-360/n)
    circle(bob,100)
    bob.lt(180)
    
m=20
def circle(t,r):
    arc(t,r,360/m)
for i in range(m):
    circle(bob,100)
    bob.lt(180-360/m)
    circle(bob,100)
    bob.lt(180)
    
turtle.mainloop()