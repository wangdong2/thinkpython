'''
姓名：臧志真
班级：大数据一班
学号：202105755928
'''
import math
import turtle
bob=turtle.Turtle()
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def polygon(t,n,length):
    angle=360/n
    polyline(t,n,length,angle)
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def part(t, r, angle):
    for i in range(2):
        arc(t, r, angle)
        t.lt(180-angle)
def flower(t, n, r, angle):
    for i in range(n):
        part(t, r, angle)
        t.lt(360.0/n)
def blank(t,length):
    t.pu()
    t.fd(length)
    t.pd()
flower(bob,7,60,60)
blank(bob,130)
flower(bob,10,45,80)
blank(bob,130)
flower(bob,20,150,20)
turtle.mainloop()