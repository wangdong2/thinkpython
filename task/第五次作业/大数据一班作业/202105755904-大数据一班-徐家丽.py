#姓名：徐家丽
#学号：202105755904
#班级：数据科学与大数据技术一班
import math
import turtle
bob=turtle.Turtle()
def polyline(t,n,length,angle):#多边线；t:乌龟；n：步数；length：每步的长度；angle：每次向左转的度数
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(t,r,angle):#圆弧；t：乌龟；r：圆弧半径；angle：圆弧角度
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def flower(t,m):#花朵；t：乌龟；m：花瓣个数
    t.pd()
    h=100
    if m<12:
        k=60
    if m>=12:
        k=90-180/m
    r=h/(2*math.cos(k*(math.pi/180)))
    for i in range(m):
        t.lt(k)
        arc(t,r,180-2*k)
        t.lt(2*k)
        arc(t,r,180-2*k)
        t.lt(k+360/m)
    t.pu()
bob.pu()
bob.fd(-200)
flower(bob,7)#七个花瓣的花
bob.fd(200)
flower(bob,10)#十个花瓣的花
bob.fd(200)
flower(bob,20)#二十个花瓣的花
turtle.mainloop()