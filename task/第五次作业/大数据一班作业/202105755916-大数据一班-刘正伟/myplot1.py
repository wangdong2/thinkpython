import turtle

bob = turtle.Turtle()
print(bob)
bob.delay=0.01

import math

def circle(t,r,circleangle):
    circlelength = 2*math.pi*r*circleangle/360
    n = int(circlelength/3)+1
    length = circlelength/n
    angle = circleangle/n
    for i in range(7):
        for j in range(n):
            t.bk(length)
            t.rt(angle)
        t.rt(360-360/7)
        for i in range(n):
            t.fd(length)
            t.rt(angle)
    
circle(bob,200,360/7)
turtle.mainloop()

    