

import turtle
import math

#调用乌龟画图、提高画弧速度
bob = turtle.Turtle()
bob.delya = 0.01

#多边线
def polyline(t, n, length, angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)

#弧
def arc(t, angle, arc_length):
    #计算：弧分几段画，每段多长，每段之间的夹角
    n = int(arc_length / 4) + 3
    step_length = arc_length / n
    step_angle = float(angle) / n
    #画出弧
    t.lt(step_angle/2)
    polyline(t, n, step_length, step_angle)
    t.rt(step_angle/2)

#不重叠花瓣的花朵
def flower(l,m):
    #计算：弧度、弧半径、弧长
    arc_angle = 2 * math.pi / m
    arc_angle2 = 360 / m
    arc_r = l / 2 / math.sin(arc_angle/2)
    arc_length = arc_angle * arc_r
    #计算：花瓣尖转角度数
    angel = 180 - 360.0/m
    for i in range(m):
        arc(bob, arc_angle2, arc_length)
        bob.lt(angel)
        arc(bob, arc_angle2, arc_length)
        bob.lt(180)

#重叠花瓣的花朵
def flower2(l,n):
    m = int(n/2)
    flower(l,m)
    bob.lt(180/m)
    flower(l,m)
    bob.rt(180/m)

#左移240，画出不重叠花瓣的7瓣花
bob.pu()
bob.fd(-240)
bob.pd()
flower(100,7)

#右移240，画出重叠花瓣的10瓣花
bob.pu()
bob.fd(240)
bob.pd()
flower2(100,10)

#左移240，画出不重叠花瓣的20瓣花
bob.pu()
bob.fd(240)
bob.pd()
flower(100,20)

#下移120，让turtle不遮挡花朵
bob.pu()
bob.rt(90)
bob.fd(120)

turtle.mainloop()
