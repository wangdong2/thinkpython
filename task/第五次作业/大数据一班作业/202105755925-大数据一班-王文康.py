#202105755925
#大数据一班
#王文康


import turtle
bob=turtle.Turtle()
turtle.delay(0.01)
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
import math
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def petal(t,r,angle):
    for i in range(2):
        arc(t,r,angle)
        t.lt(180-angle)
def flower(t,n,r,angle):
    for i in range(n):
        petal(t,r,angle)
        t.lt(360.0/n)

def move(t, length):
    t.pu()
    t.fd(length)
    t.pd()
flower(bob,10,40,80)
move(bob,200)
flower(bob,20,140,20)
move(bob,-400)
flower(bob,7,60,60)

turtle.mainloop()