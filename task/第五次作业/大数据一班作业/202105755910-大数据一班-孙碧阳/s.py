import turtle
import math
bob = turtle.Turtle()
bob.delay = 0.001
def arc(t, r, angle):  # 角度制
    """Draws an arc with the given radius and angle.
    t: Turtle
    r: radius
    angle: angle subtended by the arc, in degrees
    """
    arc_length = 2 * math.pi * r * abs(angle) / 360
    n = int(arc_length / 4) + 3
    step_length = arc_length / n
    step_angle = float(angle) / n
 
    # making a slight left turn before starting reduces
    # the error caused by the linear approximation of the arc
    t.lt(step_angle/2)
    polyline(t, n, step_length, step_angle)
    t.rt(step_angle/2)
 
 
def petal(t, r, angle):  # 绘制花瓣
    for i in range(2):
        arc(t, r, angle)  # 画花瓣的第一笔
        t.lt(180-angle)   # 向左转180-angle度,然后画第二笔
 
 
def flower(t, n, r, angle):
    for i in range(n):
        petal(t, r, angle)
        t.lt(360/n)
 
 
def move(t, length):
    t.pu()  # pen up:抬笔
    t.fd(length)
    t.pd()  # pen down:落笔
 
move(bob, -100)
flower(bob, 7, 60.0, 40.0)
move(bob, 100)
flower(bob, 7, 60.0, 60.0)
move(bob, 200)
flower(bob, 7, 60.0, 80.0)
 
bob.hideturtle()
 
turtle.mainloop()