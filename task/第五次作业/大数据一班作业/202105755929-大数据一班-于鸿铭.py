import turtle
import math
def polyline(t,n,l,angle):
    for i in range(n):
        t.fd(l)
        t.lt(angle)
    for i in range(n):
        t.bk(l)
        t.rt(angle)

def polygon(t,n,l):
    angle=360/n
    polyline(t,n,l,angle)
    
def arc(t,r,angle,x):
    arc_l=2*math.pi*r*angle/360
    n=int(arc_l/3)+1
    step_l=arc_l/n
    o=int(x/3)
    step_angle=float(angle)/(n*o)
    polyline(t,r,step_l,step_angle)

def circle(t,r,x):
    for i in range(x):
        t.lt(360/x)
        arc(t,r,360,x)
    

bob=turtle.Turtle()
print(bob)
circle(bob,50,9)#9为花瓣瓣数，可为任意值
#此代码可制造任意花朵，但不知为何花瓣不成瓣，全是线？