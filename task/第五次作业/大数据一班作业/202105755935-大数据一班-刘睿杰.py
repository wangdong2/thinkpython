作业4.2
import turtle
import math
bob=turtle.Turtle()
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=angle/n
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
def shuai(r,m):
    for i in range(m):
        arc(bob,r,360/m)
        bob.lt(180-360/m)
        arc(bob,r,360/m)
        bob.lt(180)

shuai(100,7)


作业4.3
import turtle
import math
#设该图形n个三角形组成，三角形边长为m
bob=turtle.Turtle()
def triangle(n,m):
    for i in range(n):
        bob.fd(m)
        bob.lt(90+180/n)
        bob.fd(2*m*math.sin(1/n*math.pi))
        bob.lt(90+180/n)
        bob.fd(m)
        bob.lt(180)
triangle(20,100)#括号内为三角形个数和每个三角形边长
turtle.Turtle()

