#!/usr/bin/env python
# coding: utf-8

# # 班级：大数据二班
# # 学号：202105756007
# # 姓名：王伟丰

# In[1]:


import math
import turtle
bob=turtle.Turtle()


# In[2]:


def flower(t,n):
    for i in range(n):
        for i in range(50):
            t.fd(0.5*2*math.pi)
            t.lt(360/(50*n))
        t.lt(180-360/n)
        for i in range(50):
            t.fd(0.5*2*math.pi)
            t.lt(360/(50*n))
        t.lt(180)


# # 第一个图案

# In[ ]:


print(bob)
flower(bob,7)


# # 第二个图案

# In[3]:


print(bob)
flower(bob,5)
bob.lt(180/5)
flower(bob,5)


# # 第三个图案

# In[3]:


print(bob)
flower(bob,20)

