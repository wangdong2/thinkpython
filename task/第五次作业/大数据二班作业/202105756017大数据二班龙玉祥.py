#学号:202105756017
#班级:大数据二班
#姓名:龙玉祥
import turtle
t=turtle.Turtle()
def go(s):
    t.pu()
    t.fd(s)
    t.pd()
def petal(r,angle):
    for i in range(2):
        t.circle(r,angle)
        t.lt(180-angle)
def flower(n,r,angle):
    for i in range(n):
        petal(r,angle)
        t.lt(360/n)
go(-300)
flower(7,100,360/7)
go(300)
flower(10,80,80)
go(300)
flower(20,140,30)
turtle.mainloop()