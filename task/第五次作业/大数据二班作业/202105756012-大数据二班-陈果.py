"""
学号 202105756012
姓名 陈果
班级 大数据二班
"""

import turtle as t

def petal(t,r,angle):
    for i in range(2):
        t.circle( r, angle)
        t.lt(180-angle)
def flower(t, n, r, angle):
    for i in range(n):
        t.speed(0)
        petal(t, r, angle)
        t.lt(360/n)

def move(t, length):
    t.pu()
    t.fd(length)
    t.pd()

move(t, -200)
flower(t, 7, 100, 60)
move(t, 200)
flower(t, 10, 70, 80)
move(t, 200)
flower(t, 20, 220, 20)
t.mainloop()

