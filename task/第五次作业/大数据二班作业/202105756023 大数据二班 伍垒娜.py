大数据2班 伍垒娜 202105756023

import turtle 
import math
bob=turtle.Turtle()
bob.delay=0.01

def degree(x,r,angle): 
    degree_length=2*math.pi*r*angle/360
    n=int(degree_length/4)+3
    step_length=degree_length/n
    step_angle=float(angle)/n
    x.lt(step_angle/2)
    polyline(x,n,degree_length)
    x.rt(step_angle/2)
    
def make(x,r,angle):
    for i in range(2):
        degree(x,r,angle)
        x.lt(180)

def flower(x,n,r,angle):
    for i in range(n):
        make(x,r,angle)
        x.lt(360/n)
        
def move(x,degree_length):
    x.pu()
    x.fd(degree_length)
    x.pd()
    
move(bob,100)
flower(bob,7,40,40)
move(bob,100)
flower(bob,7,40,60)
move(bob,200)
flower(bob,7,40,80)

bob.hideturtle

turtle.mainloop()
