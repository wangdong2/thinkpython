# 姓名：刘烨
# 班级：数据科学与大数据技术2班
# 学号：202105756034
import math
import turtle
bob=turtle.Turtle()
bob.delay = 0.01
def polyline(t,n,length,angle):
    ###
    #画多边形函数,接受四个参数t:turtle名称; n:步数; length:每步长度; angle:每次向左转的度数
    ###
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(t,r,angle):
    ###
    #画圆弧函数,接受四个参数t:turtle名称; r:圆弧半径; angle:圆弧对应角度
    ###
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def flower(t,h,n):
    ###
    #画花朵函数, 接受三个参数t:turtle名称; h:花朵大小; n:花瓣个数
    ###
    t.pd()
    if n>=1 and n<11:
        a=60
    if n>=11:
        a=90-200/n
    b=a*(math.pi/180)
    c=360/n
    r=h/(2*math.cos(b))
    for i in range(n):
        t.lt(a)
        arc(t,r,180-2*a)
        t.lt(2*a)
        arc(t,r,180-2*a)
        t.lt(a+c)
    t.pu()
def threeflowers(t,h1,n1,h2,n2,h3,n3):
    ###
    #画三个花朵函数，接受七个参数t:turtle; h1、n1为第一个花朵的大小和花瓣个数, h2、n2为第二个花朵的大小和花瓣个数, h3、n3为第三个花朵的大小和花瓣个数。
    ###
    length=500
    t.pu()
    t.fd(-length)
    flower(bob,h1,n1)
    t.fd(length)
    flower(bob,h2,n2)
    t.fd(length)
    flower(bob,h3,n3)
threeflowers(bob,200,7,200,10,200,20)# 这里演示的是书上的三个花朵
turtle.mainloop()
    

    