import math
import turtle

bob = turtle.Turtle()

def polyline(t, n, length, angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)

def arc(t, angle, arc_length):
   n = int(arc_length / 4) + 3
   step_length = arc_length / n
   step_angle = float(angle) / n
   t.lt(step_angle / 2)
   polyline(t, n, step_length, step_angle)
   t.rt(step_angle / 2)
      
def flower1(l, m):
   arc_angle = 2 * math.pi / m
   arc_angle2 = 360 / m
   arc_r = l / 2 / math.sin(arc_angle / 2)
   arc_length = arc_angle * arc_r
   angle = 180 - 360 / m
   for i in range(m):
      arc(bob, arc_angle2, arc_length)
      bob.lt(angle)
      arc(bob, arc_angle2, arc_length)
      bob.lt(180)
             
def flower2(l, n):
   m = int(n / 2)
   flower1(l, m)
   bob.lt(180 / m)
   flower1(l, m)
   bob.rt(180 / m)
             
bob.pu()
bob.fd(-240)
bob.pd()             
flower1(100, 7)

bob.pu()
bob.fd(240)
bob.pd()             
flower2(100, 10)

bob.pu()
bob.fd(240)
bob.pd()             
flower1(100, 20)

bob.pu()
bob.rt(90)
bob.fd(120)

turtle.mainloop()  