import turtle
import math

bob = turtle.Turtle()

def arc(t,r,k,m,x):
    angle=360/k
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=angle/n
    for l in range(x):
        for j in range(k):   
            for i in range(n):
                t.fd(step_length)
                t.lt(step_angle)
            t.lt(180-360/k)
            for i in range(n):
                t.fd(step_length)
                t.lt(step_angle)
            t.lt(m)
    
    
arc(bob,100,7,180,1)
bob.fd(250)
arc(bob,80,6,150,2)
bob.fd(250)
arc(bob,350,20,180,1)

turtle.mainloop()   
    
    