#4.2

'''通过观察题目发现，该图形其实是由不同个数的花瓣组成的，所以我们需要知道花瓣怎么花；而花瓣其实就是两个对称的圆弧，所以我们可以用circle来画圆弧，最后直接for   in函数循环就可以画出花朵'''
import turtle as tl
import math
tl.speed(10)#画图时间太长，我们直接把画笔速度拉满
#首先我们先定义一个弧线的函数，因为花瓣就是由圆弧组成的
def arc(tl,r,angle):#此圆弧函数是以多边形的基础来建立的，也就是书上给的，但是我们可以用画圆的公式来实现函数的简化，我用注释放在代码的最后面。
    c=2*math.pi*r*angle/360#c是该圆弧的弧长，此公式就是求弧长的公式。
    n=int(c/3+3)
    length=c/n
    for i in range(n):
        tl.fd(length)
        tl.lt(angle/n)
#然后我们定义一个花瓣（petal）的函数
def petal(t,s,angle):
    r=s/2/math.sin(angle/2*math.pi/180)
    arc(tl,r,angle)
    tl.lt(180-angle)
    arc(tl,r,angle)
    tl.lt(180-angle)
#最后我们定义一个花的函数就ok了
def flower(t,n,m,r):
    #m为花瓣重叠的个数
    center_angle=360*m/n
    for i in range(n):
        petal(tl,r,center_angle)
        tl.lt(360/n)
#函数已经构建完成，现在开始画图了
#第一个图有七个花瓣
for i in range(7):
    petal(tl,100,360/7)#这里弧长调小是因为画的太慢，花小一点会快一点，开始调的100画了好久
    tl.lt(360/7)
#观察发现，第一个和第三个是一种类型，所以我们先画第三个    
#第三个图形有个花瓣，但是为了防止花重合，我们需要将画笔移开一点
tl.penup()
tl.fd(200)
tl.pendown()
for i in range(20):
    petal(tl,100,360/20)
    tl.lt(360/20)
#画有二十个花瓣的花弧长太小已经有些看不清了，所以我还是把弧长改了回去
tl.penup()#此时和上面操作的意义是一样的，防止图案重合
tl.lt(180)
tl.fd(400)
tl.pendown()
'''观察发现第二个图与第一，第三不一样，有重叠的部分，但是花瓣的个数还是符合上面的，就是在画完花瓣后的转向问题需要修改，而通过几何的观察可以总结出转向的区别从而写出代码'''
for i in range(10):
    petal(tl,100,360*2/10)
    tl.lt(360/10)
tl.mainloop()
    