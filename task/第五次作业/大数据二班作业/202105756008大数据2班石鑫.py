#写一组适合的通用函数，用来花出花朵图案
import turtle
t=turtle.Turtle()
import math

def arc(t,r,y,m):
    angle=360/y#y为每个花瓣尖端的角度
    h=y*m#h为最大花瓣的个数 m为花朵的数量
    for f in range(h):
        for g in range(2):
            arc_lengle=2*math.pi*r*angle/360
            n=int(arc_lengle/3)+1
            step_lengle=arc_lengle/n
            step_angle=float(angle)/n
    
            for i in range(n):
                t.fd(step_lengle)
                t.lt(step_angle)
            t.lt(180-angle)
        t.rt(angle/m)
          
arc(t,100,6,2)
turtle.mainloop()