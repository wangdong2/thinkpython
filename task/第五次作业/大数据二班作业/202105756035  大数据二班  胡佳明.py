"""
202105756035

大数据二班

胡佳明
"""



import turtle as t

def print_flower(a,b,c):# a为花瓣个数，b为花瓣圆弧所对圆的半径，c为圆弧转过的角度
    
    for i in range (a):
        t.speed(0)
        t.circle(b,c)
        t.lt(180-c)
        t.circle(b,c)
        t.lt(180-c)
        t.lt(360/a)
        
def move(t,length):# length为花朵左右方向的移动距离
    t.pu()
    t.fd(length)
    t.pd()
    
move(t,-200)
print_flower(7,80,60,)

move(t,200)
print_flower(10,80,70)

move(t,200)
print_flower(20,100,30)

t.mainloop()
    