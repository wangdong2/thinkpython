#202105756027
#大数据二班
#丛博予

#练习4-2

import turtle as t
def print_flower(a,b,r):#a=单个花瓣圆弧的圆心角，b=花瓣的个数，r=花瓣圆弧的半径
    for i in range(b):
        t.circle(r,a)
        t.lt(180-a)
        t.circle(r,a)
        t.lt(180-a)
        t.lt(360/b)
def move(t,length):#定义新函数
    t.pu()
    t.fd(length)
    t.pd()
t.speed(0)#设置速度

move(t,-200)#开始运行
print_flower(60,7,100)
move(t,200)
print_flower(60,10,100)
move(t,200)
print_flower(20,20,250)

t.mainloop()
   
   
     
               