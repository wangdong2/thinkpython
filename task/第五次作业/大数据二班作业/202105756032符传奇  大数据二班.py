import math
import turtle
def arc(t,r,angle):#角度以及一小段线段长
    arc_length = 2*math.pi*r*angle/360
    n = int(arc_length/3+3)
    step_length = arc_length/n
    step_angle = angle/n
    for i in range(n):
        t.lt(step_angle)
        t.fd(step_length)
        
def petal(t,s,angle):#花瓣绘制
    r = s/ 2 / math.sin(angle/2*math.pi/180)
    arc(t,r,angle)
    t.lt(180-angle)
    arc(t, r,angle)
    t.lt(180-angle)

bob = turtle.Turtle()

for i in range(7):#图形一
    petal(bob,60,360/7)
    bob.lt(360/7)
    
for i in range(10):#图形二
    petal(bob,60,72)
    bob.lt(36)
       
for i in range(20):#图形三
    petal(bob,60,18)
    bob.lt(18)     

   

    
turtle.mainloop()

   