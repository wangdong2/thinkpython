"""202105756020徐坚大数据二班"""

import turtle
import math
bob = turtle.Turtle()

def arc(t,r,angle,):
    c = 2*math.pi*r *angle/360
    n =int(c/3+3)
    length = c/n
    for i in range(n):
        t.fd(length)
        t.lt(angle/n)
        
        
def petal(t, s, angle):

   r = s/ 2 / math.sin(angle/2*math.pi/180)
   arc(t,r,angle)
   t.lt(180-angle)
   arc(t, r,angle)
   t.lt(180-angle)
   """
   t:turtle对象
   s：弦长
   angle：切线角 
   """

#petal(bob,200,60)



def flower(t,n,m,r):

   center_angle =360*m/n
   for i in range(n):
    petal(t,r,center_angle)
    t.lt(360/n)
   """
   n int ，为花瓣数；
   m为重叠层数
   r为图案半径 
   """ 

turtle.delay(0.01)
bob.penup()
bob.goto(-200,100)
bob.pendown()
for i in range(7):
    petal(bob,60,360/7)
    bob.lt(360/7)


bob.penup()
bob.goto(200,-200)
bob.pendown()    

  
for i in range(20):
    petal(bob,60,360/20)
    bob.lt(360/20)
    
bob.penup()
bob.goto(0,0)
bob.pendown()   
    
    
    

for i in range(10):
    petal(bob,60,360*2/10)
    bob.lt(360/10)
   
     
        
bob.ht()

    




turtle.mainloop()

