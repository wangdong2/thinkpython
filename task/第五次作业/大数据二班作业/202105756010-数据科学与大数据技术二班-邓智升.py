import turtle
bob=turtle.Turtle()
print(bob)
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
        
import math
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
    
def circle(t,r,m,x):
    arc(t,r,360*x/m)

def flower(t,r,m,x,d):
    for i in range(m):    
        circle(t,r,m,x)
        t.lt(180-360*x/m)
        circle(t,r,m,x)
        t.lt(180-360*x*d/m)
        
flower(bob,100,10,1,0)
flower(bob,100,10,2,1/2)


turtle.mainloop()