import turtle
import math
bob = turtle.Turtle()
bob.delay =0.01
def arc(t, r, angle):  
    arc_length = 2 * math.pi * r * abs(angle) / 360
    n = int(arc_length / 4) + 3
    step_length = arc_length / n
    step_angle = float(angle) / n
 
    t.lt(step_angle/2)
    polyline(t, n, step_length, step_angle)
    t.rt(step_angle/2)

def petal(t, r, angle):  
    for i in range(2):
        arc(t, r, angle) 
        t.lt(180-angle) 
        
def flower(t, n, r, angle):
    for i in range(n):
        petal(t, r, angle)
        t.lt(360/n)
        
def move(t, length):
    t.pu()  
    t.fd(length)
    t.pd() 
    
move(bob, -100)
flower(bob, 7, 60, 40)
move(bob, 100)
flower(bob, 7, 60, 60)
move(bob, 200)
flower(bob, 7, 60, 80)

turtle.mainloop()