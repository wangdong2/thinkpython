# 姓名：曹佳俊
# 学号：202105756004
# 班级：数据科学与大数据技术二班
import math
import turtle
bob=turtle.Turtle()
turtle.delay(2)
def polyline(t,n,length,angle):
    '''
    画多边线函数,设置四个参数
    t:turtle名称     n:步数    length:每步长度    angle：每次向左转的度数
    '''
    for i in range(n):
        t.fd(length)
        t.lt(angle)
def arc(t,r,angle):
    '''
    画圆弧函数，设置四个参数
    t：turtle名称；   r：圆弧半径    angle：圆弧对应角度
    '''
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def flower(t,d,n):
    '''
    画花朵函数，设置三个参数
    t：turtle名称    d：花朵大小    n：花瓣个数
    '''
    t.pd()
    if n>=1 and n<11:
        a=60
    if n>=11:
        a=90-200/n
    b=a*(math.pi/180)
    c=360/n
    r=d/(2*math.cos(b))
    for i in range(n):
        t.lt(a)
        arc(t,r,180-2*a)
        t.lt(2*a)
        arc(t,r,180-2*a)
        t.lt(a+c)
    t.pu()
def threeflower(t,h1,n1,h2,n2,h3,n3):
    '''
    画三朵花朵函数，接受七个参数t
    turtle名称    h1,n1分别为第一朵花的大小和花瓣数，h2，n2,h3,n3同理
    '''
    length=250
    t.pu()
    t.fd(-length)
    flower(bob,h1,n1)
    t.fd(length)
    flower(bob,h2,n2)
    t.fd(length)
    flower(bob,h3,n3)
threeflower(bob,100,7,100,10,100,20)
turtle.mainloop()