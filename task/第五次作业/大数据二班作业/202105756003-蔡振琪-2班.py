import turtle
t=turtle.Turtle()
print(t)
t.delay=0.01
def flower(n):   #n表示瓣数
    c=int(720/n)
    for p in range(n):
        for i in range(c):
            t.lt(0.5)
            t.fd(180/c)
        t.rt(0.5*c)
        for j in range(c):
            t.lt(0.5)
            t.bk(180/c)    
        t.lt(360/n-0.5*c)
flower(10)
turtle.mainloop()