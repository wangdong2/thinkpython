"""
姓名：齐异
班级：大数据二班
学号：202105756026
"""
import math
import turtle
bob = turtle.Turtle()
turtle.delay(0) #加速乌龟爬行速度
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
        
def polygon(t,n,length):
    angle=360/n
    polyline(t,n,length,angle)
    
def arc(t,r,angle):
    arc_length=2*math.pi*r*angle/360
    n = int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
    
def huaban(t,d,angle):
    """
    定义弦距为d
    定义弦切角为angle
    """
    r = d / 2/math.sin(angle/2*math.pi/180)
    arc(t,r,angle)
    t.lt(180 - angle)
    arc(t,r,angle)
    t.lt(180 - angle)
    
def hua(t,n,m,r):
    """
    定义n为花瓣个数
    定义m为花瓣重叠层数
    定义r为圆弧半径长度
    """
    for i in range(n):
        huaban(t,r,360*m/n)
        t.lt(360/n)
        
def move(t,length): #令每一朵花移动位置
    t.pu()
    t.fd(length)
    t.pd()
    
move(bob,-200)
hua(bob,7,1,100)
move(bob,200)
hua(bob,10,2,100)
move(bob,200)
hua(bob,20,1,100)


bob.hideturtle() #隐藏乌龟的箭头
turtle.mainloop()