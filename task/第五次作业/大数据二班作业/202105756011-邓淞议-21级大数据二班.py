import turtle
import math
bob = turtle.Turtle()

def arc(t,r,angle,):
    c = 2*math.pi*r *angle/360
    n =int(c/3)+1
    length = c/n
    for i in range(n):
        t.fd(length)
        t.lt(angle/n)
        
        
def polygon(t, s, angle):
   r = s/ 2 / math.sin(angle/2*math.pi/180)
   arc(t,r,angle)
   t.lt(180-angle)
   arc(t, r,angle)
   t.lt(180-angle)


def flower(t,n,angle,s):
   center_angle =360/n
   r = s/ 2 / math.sin(angle/2*math.pi/180)
   for i in range(n):
      polygon(t,r,center_angle)
      t.lt(360/n)
    
turtle.delay(0.01)

flower(bob,7,360/7,600/7)
   
bob.fd(200)    

flower(bob,10,360/10,600/10)


bob.fd(200)       
flower(bob,20,360/20,600/20)     

    
turtle.mainloop()

