#202105756021  大数据2班  康丹琪
import turtle 
import math
tom=turtle.Turtle()
tom.delay=0.00000000001
def polyline(x,n,degree_length):
    for i in range(n):
        angle=360/n
        x.fd(degree_length)
        x.lt(angle)
def degree(x,r,angle):#x=turtle  r=radius  
    degree_length=2*math.pi*r*angle/360
    n=int(degree_length/4)+1
    step_length=degree_length/n
    step_angle=float(angle)/n
    x.lt(step_angle/2)
    polyline(x,n,degree_length)
    x.rt(step_angle/2)
    
def make(x,r,angle):#画花瓣 
    for i in range(2):
        degree(x,r,angle)
        x.lt(180)

def flower(x,n,r,angle):
    for i in range(n):
        make(x,r,angle)
        x.lt(360/n)
        
def start(x,degree_length):
    x.pu()#开始画画
    x.fd(degree_length)
    x.pd()#停止画画
    
start(tom,100)
flower(tom,7,40,40)
start(tom,100)
flower(tom,7,40,60)
start(tom,200)
flower(tom,7,40,80)

tom.hideturtle

turtle.mainloop()

        
        
        
