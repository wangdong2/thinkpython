import turtle
bob = turtle.Turtle()
import math
def polyline(t, n, length, angle):
    for i in range (n):
        t.fd(length)
        t.lt(angle)

def polygon(t, r, length):
    angle = 360.0 / n
    polyline(t, n, length, angle)

def arc(t, r, angle):
    arc_length = 2 * math.pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_angle = float(angle) / n
    step_length = arc_length / n
    polyline(t, n, step_length, step_angle)

def circle(t, r):
    arc(t, r, 360)

def flower(bob, a, n):
    for i in range(n):
        b = 720 // n
        c = 180 - (b / 2)
        d = 180 - b
        arc(bob, a, b) 
        bob.lt(c)
        arc(bob, a, b)
        bob.lt(d)

flower(bob, 100, 20)
bob.fd(200)
flower(bob, 100, 10)
bob.fd(200)
flower(bob, 100, 7)
turtle.mainloop()