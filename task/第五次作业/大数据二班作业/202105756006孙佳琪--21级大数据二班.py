#此函数适用于：1.不重叠花瓣的花 2.偶数花瓣数重叠花瓣的花
#下面做依次横向画出练习中三朵花的示例

import math
import turtle

bob=turtle.Turtle()
turtle.delay(0)

def sunleo(t,s,r,m): #定义函数头，t代表bob，s为所画单个不重叠的花的个数，r为所对二分之一花瓣弧所对圆心角的大小，m为一朵不重叠花瓣花的花瓣数
    angle=360/m   #angle代表每个花瓣花心处两个切线所夹角所占角度的大小
    arc_length=2*math.pi*r*angle/360
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    for k in range(s):  #s为花朵重复次数
        for j in range(m): #重复每个花瓣，出现一朵花m个花瓣
            for i in range(n):  #先画一半花瓣
                t.fd(step_length)
                t.lt(step_angle)  
            t.lt(180-angle)     #旋转光标
            for i in range(n):   #再画另一半花瓣
                t.fd(step_length)
                t.lt(step_angle)
            t.lt(180-2*angle)   #旋转，以得到不重复花瓣   
        t.lt(180-(s+1- m%2)*angle/s) #可以将光标旋转至两个花瓣中间，以出现重叠花瓣部分 

def move(t,length):#移动bob  
    t.pu()
    t.fd(length)
    t.pd()    
        
    
move(bob,-400)
sunleo(bob,1,180,7)
bob.rt(2.5/7*360)#将光标移动到正右向
move(bob,400)
sunleo(bob,2,130,5)
bob.rt(6/10*360)#将光标移动到正右向
move(bob,400)
sunleo(bob,1,480,20)
turtle.mainloop()
