"""
作业从80行开始   丁宇宏 大数据二班 202105756013 turtle 可以用as命名但是会不太方便不如bob=好用

"""




import turtle
bob = turtle.Turtle()

import math

def square(t, length):
    for i in range(4):
        t.fd(length)
        t.lt(90)
        
        
        
def polygon(t, length, n):
    for i in range(n):
        t.fd(length)
        t.lt(360/n)
#square(bob,200)

def circle(t,r):
    c = 2*math.pi*r 
    n=500  # n 边数
    length= c /n  # 边长等于周长除半径
    polygon=(t,length,n) 
    
circle(bob,100)    
    
    
def arc(t,r,angle,lr='l'):
    c = 2*math.pi*r *angle/360
    n = int(c/3+3)
    length=c/n
    polyline(t,length,angle/n,lr)
  

 

def polyline(t, length, n, angle,lr='l'):
    for i in  range(n):
        t.fd(length)
        if lr=="l":
            
            t.lt(angle)
        elif lr=='r':
            t.rt(angle)
            
            
            
            
            
def taiji(t,r):
    arc(bob,r/2,180,'r')
    arc(bob,r/2,180,'l')
    circle(bob,r)    #"""出了问题circle掉不出来等下写完作业在查一下原因 """
    

#square(bob,200)


#polygon(bob,3,300)
turtle.delay(0.01)  #bob.dalay=0.01在3.8.2中已经失效，使用turtle.delay（0.01）代替
 #circle(bob,100)
 #arc(bob, 100,180,'r')
 #arc(bob, 100,180,'l')
 #arc(bob, 200,360,'l') # 乌龟 半径 角度 左右 或者使用关键字来控制不需要符合顺序

#polyline(bob,20,50,30,'r')

#taiji(bob,100)
#circle(bob,100)

"""作业"""

def arc(t,r,angle,):
    c = 2*math.pi*r *angle/360
    n =int(c/3+3)
    length = c/n
    for i in range(n):
        t.fd(length)
        t.lt(angle/n)
        
        
def petal(t, s, angle):
   """
   t:turtle对象
   s：弦长
   angle：切线角 
   """
   r = s/ 2 / math.sin(angle/2*math.pi/180)
   arc(t,r,angle)
   t.lt(180-angle)
   arc(t, r,angle)
   t.lt(180-angle)


#petal(bob,200,60)









def flower(t,n,m,r):
   """
   该注释下的代码与该符号一列否则会报错
   
   用乌龟t画n个半径为r的花瓣图案
   t 乌龟名；
   n int ，为花瓣数；
   m为重叠层数
   r为图案半径 
   """ 
   center_angle =360*m/n
   for i in range(n):
    petal(t,r,center_angle)
    t.lt(360/n)
    
bob.ht()    
"""bob.color('green')   
#第一个图形    
for i in range(7):
    petal(bob,60,360/7)
    bob.lt(360/7)
bob.color('red')


bob.penup()
bob.goto(100,100)
bob.pendown()
    
#第二个图形   
for i in range(20):
    petal(bob,60,360/20)
    bob.lt(360/20)
    
bob.color('gray',)    
bob.penup()
bob.goto(-100,-100)
bob.pendown()    
    

    
#第三个图形 
for i in range(10):
    petal(bob,60,360*2/10)
    bob.lt(360/10)       
"""

# 如果像上面一样过于麻烦所以引入新的函数来简化一下过程

def tu(t,a,b,lr='l'):
    if lr=='l':
        for i in range(b):
            c=360/b
            petal(bob,a,c)
            bob.lt(c)
    if lr=='r':
        for i in range(10):
            petal(bob,60,360*2/10)
            bob.lt(360/10)

bob.penup()
bob.goto(-100,-100)
bob.pendown()
#第一题
bob.color('green')
tu(bob,60,7,'l')
bob.color('red')
bob.penup()
bob.goto(0,0)
bob.pendown()
#第二题
tu(bob,60,20,'l')
bob.color('gray',)    
bob.penup()
bob.goto(100,100)
bob.pendown()
#第三题
tu(bob,60,10,'r')
    

     

    








turtle.mainloop()
    
        

