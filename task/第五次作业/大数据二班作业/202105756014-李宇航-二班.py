#*姓名：李宇航
#*学号：202105756014
#*班级：数据科学与大数据技术二班
import turtle
bob = turtle.Turtle()
print(bob)
def print_square (a,b,c):
# a为花的花瓣个数
#b为圆弧所对圆的半径
#c为圆弧所对的角度
    for i in range (a):
        bob.circle(b,c)
        bob.lt(180-c)
        bob.circle(b,c)
        bob.lt(180-c)
        bob.lt(360/a)
print_square(7,80,60)
print_square(10,80,70)
print_square(20,100,30)   
turtle.mainloop()