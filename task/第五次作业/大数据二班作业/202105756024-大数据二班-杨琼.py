# p39 练习4-2 写一组合适的通用函数，用来画出图4-1所示的花朵图案

"""
x: 表示花瓣直径
y: 表示花瓣数量
"""
import math
import turtle
bob = turtle.Turtle()
bob.delya=0.01

"""
画出花瓣边线
"""
def polyline(t,n,length,angle):
    for i in range(n):
        t.fd(length)
        t.lt(angle)
"""
计算花瓣弧度
"""
def arc(t,angle,arc_length):
    n=int(arc_length/4)+3
    step_length=arc_length/n
    step_angle=float(angle)/n
    t.lt(step_angle/2)
    polyline(t,n,step_length,step_angle)
    t.rt(step_angle/2)
    
"""
画出花瓣不重叠的花朵，即图一与图三的通用函数
"""
def flower(x,y):
    arc_angle=2 * math.pi /y
    arc_angle2=360/y
    arc_r=1/2/math.sin(arc_angle/2)
    arc_length=arc_angle*arc_r
    angle=180-360.0/y
    for i in range(y):
        arc(bob,arc_angle2,arc_length)
        bob.lt(angle)
        arc(bob,arc_angle2,arc_length)
        bob.lt(180)
        
"""
画出花瓣重叠的花朵，即图二
"""
def flower2(x,n):
    x=int(n/2)
    flower(x,y)
    bob.lt(180/y)
    flower(x,y)
    bob.rt(180/y)   
"""
画出不重叠的7朵花瓣
"""
bob.pu()
bob.fd(-240)
bob.pd()
flower(100,7)
"""
画出重叠的10朵花瓣
"""
bob.pu()
bob.fd(240)
bob.pd()
flower2(100,10)
"""
画出不重叠的20朵花瓣
"""
bob.pu()
bob.fd(240)
bob.pd()
flower(100,20)
"""
使turtle不遮挡花朵
"""
bob.pu()
bob.rt(90)
bob.fd(120)
    
turtle.mainloop()
        
        
    

         




        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        