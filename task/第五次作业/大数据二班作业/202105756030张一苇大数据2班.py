#姓名：张一苇
#班级：2021数据科学与大数据技术2班
#学号：202105756030
"""
题目：写一组合适的通用函数，用来画出图4-1所示的花朵图案。
"""
import math
import turtle                     #调用数学、turtle模块
bob = turtle.Turtle()
bob.delay=0.01                    #加速乌龟运行

def polyline(t,n,length,angle):         #定义多边线函数，以t,n,length,angle为4个形参（t:turtle，n:前进次数，length：前进长度，angle：左转的角度）
    for i in range(n):              #重复n次
        t.fd(length)
        t.lt(angle)
def polygon(t,n,length):             #定义多边形函数
    angle=360.0/n
    polyline(t,n,length,angle)        
def arc(t,r,angle):                 #定义弧函数，以t,angle,arc_length为三个形参（t:turtle,r:半径，angle：角度）
    arc_length=2*math.pi*r*angle/360    #弧长=2pi*r*角度（弧度制）
    n=int(arc_length/3)+1
    step_length=arc_length/n
    step_angle=float(angle)/n
    polyline(t,n,step_length,step_angle)
def petal(t,r,angle):                 #定义花瓣函数（t:turtle,r:半径，angle：角度）
    for i in range(2):               #用两个弧线完成一个花瓣
        arc(t,r,angle)
        t.lt(180-angle)
    

def flower(t,n,r,angle):          #定义花瓣函数（用两个弧线完成一个花瓣）（t:turtle,r:半径，angle：角度）
    for i in range(n):
        petal(t,r,angle)
        t.lt(360.0/n)
    
#flower(bob,7,60,60)              #图4-2的第一朵花朵



'''图4-2的第2朵花朵：'''
#flower(bob,10,40,80)             
#图4-2的第3朵花朵：
flower(bob,20,140,20)


    
    
turtle.mainloop()        

    
